import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { ExpertService } from 'src/app/providers/expert.service';
import { GlobalService } from 'src/app/providers/global.service';

@Component({
  selector: 'app-AdminExpert',
  templateUrl: './expert.component.html',
  styleUrls: ['./expert.component.css']
})
export class AdminExpertComponent implements OnInit {

  expertData:any;
  headerimageurl:any;
  // msg:Message[]=[];
  projectionimageurl:any;
  email:string;
  constructor(private activatedRoute:ActivatedRoute,public expertservice:ExpertService,public globalservice: GlobalService,
    public messageService : MessageService,public router:Router){
      this.headerimageurl  = this.globalservice.serverip() + 'public/experts/'
      this.projectionimageurl = this.globalservice.serverip() + 'public/experts/degrees/'
  
  }
  ngOnInit() {
    this.getsingleexpert();
    // this.editarticleForm =  new FormGroup({
    //   title: new FormControl('',[Validators.required]),
    //   article :new FormControl('',[Validators.required]),
    //   summary: new FormControl('',[Validators.required,Validators.maxLength(100)]),
    //   headerimage: new FormControl('',[Validators.required]),
    //   reference: new FormControl(''),
    //   youtubelink: new FormControl('')
    // })
  }
  enable_profile(){
    (<HTMLInputElement> document.getElementById("enable_expert")).disabled=true

    const allParams = this.activatedRoute.snapshot.params // allParams is an object

    const expertid = allParams.expert_id;
    this.expertservice.approveexpert(expertid).subscribe((data:any)=>{
      if(data.status==200) {
        this.messageService.add({severity:'success', summary:'Expert enaled Successfully'});
        (<HTMLInputElement> document.getElementById("enable_expert")).disabled=false

      }
      console.log()
    },(error:any)=>{
      console.log(error);
      (<HTMLInputElement> document.getElementById("enable_expert")).disabled=false

    })
  }
  
  getsingleexpert(){
    const allParams = this.activatedRoute.snapshot.params // allParams is an object
    const expertid = allParams.expert_id
    console.log('params',allParams)
    console.log("you are getting Id in front",expertid);
    this.expertservice.getexpertuser(expertid).subscribe((data:any)=>{
      console.log("This is the data ",data);
      // this.articleData = data.articledata;
      this.expertData = data.data;
      console.log(this.expertData)
      this.email=this.expertData.email
      // this.editarticleForm.controls["title"].setValue(this.articleData.title);
      // this.editarticleForm.controls["summary"].setValue(this.articleData.summary);
      // this.editarticleForm.controls["article"].setValue(this.articleData.article);
      // this.editarticleForm.controls["reference"].setValue(this.articleData.reference);
      // this.editarticleForm.controls["youtubelink"].setValue(this.articleData.youtubelink);
    },(error:any)=>{
      console.log("Error while fetching the expert ",error);
    })
  }

}
