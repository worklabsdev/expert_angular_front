import { Component, OnInit } from '@angular/core';
import { ExpertService } from 'src/app/providers/expert.service';

@Component({
    selector: 'app-newsletter',
    templateUrl: './newsletter.component.html',
    styleUrls: ['./newsletter.component.css']
  })
export class NewsLetterComponent implements OnInit{
    // expertservice:any;
    expertisearray:string;
    dropdownList:any;
    userList:any;
    articleList:any;
    selectedItems = [];
    articleItems = [];
    userItems = [];

    dropdownSettings = {};
    tags:any;
    constructor(public expertservice:ExpertService){

    }
    ngOnInit(){
        this.getexpertise();
        // this.dropdownList = this.expertisearray;
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 42,
            allowSearchFilter: true
          };
          // (<HTMLInputElement> document.getElementById("userdiv")).style.display='none';
          // (<HTMLInputElement> document.getElementById("articlediv")).style.display='none';
          // (<HTMLInputElement> document.getElementById("senddiv")).style.display='none';
        // this.getusers();
        // this.userList = 
    }
    getexpertise(){
        // console.log(event.query);
        this.expertservice.getexpertise().subscribe((data:any)=>{
          console.log("this is the first log",data.data.length);
          this.dropdownList = data.data;
          //  for(var t=0;t<data.data.length;t++){
          // this.expertisearray.push(data.data[t]);
          //  }
          var i;
          for(i = 0; i < this.dropdownList.length; i++){
            this.dropdownList[i]['item_id'] = this.dropdownList[i]['_id'];
            this.dropdownList[i]['item_text'] = this.dropdownList[i]['expertise'];
            delete this.dropdownList[i]['_id'];
            delete this.dropdownList[i]['expertise'];
            console.log(this.dropdownList)
          }
          console.log("this is the data for expertise",this.dropdownList);
        },(error:any)=>{
          console.log(error);
        })
      }
      onItemSelect(item: any) {
        console.log(item);
        this.tags=item;
      }
      onSelectAll(items: any) {
        console.log(items);
      }
      submitdropdown(){
         console.log(this.selectedItems) ;
         this.getusersandarticles();
      }

      getusersandarticles(){
        console.log(this.selectedItems);
        var tag=[]
        for(var i =0; i< this.selectedItems.length ;i++){
          tag.push(this.selectedItems[i]['item_text']);
        }
      
        var postbody={
          tags: tag
        }
        this.expertservice.getusers(postbody).subscribe((data:any)=>{
          console.log("this is the first log",data);
          this.userList = data.list.userlist;
          this.articleList = data.list.articlelist;
          var i;
          for(i = 0; i < this.userList.length; i++){
            this.userList[i]['item_id'] = this.userList[i]['email'];
            this.userList[i]['item_text'] = this.userList[i]['name'];
            delete this.userList[i]['email'];
            delete this.userList[i]['name'];
          }
          var j;
          console.log(this.articleList)
          for(j = 0; j < this.articleList.length; j++){

            this.articleList[j]['item_id'] = this.articleList[j]['_id'];
            this.articleList[j]['item_text'] = this.articleList[j]['title'];
            delete this.articleList[j]['_id'];
            delete this.articleList[j]['name'];
          }
          (<HTMLInputElement> document.getElementById("userdiv")).style.display='block';
          (<HTMLInputElement> document.getElementById("articlediv")).style.display='block';
          (<HTMLInputElement> document.getElementById("senddiv")).style.display='block';

          console.log("this is the data for user",data.list.userlist);
        },(error:any)=>{
          console.log(error);
        })
      }
     

      onUserSelect(item: any) {
        console.log(item);
        this.tags=item;
      }
      onSelectAllUser(items: any) {
        console.log(items);
        this.tags=items
      }
     
      onArticleSelect(item: any) {
        console.log(item);
        this.tags=item;
      }
      onSelectAllArticle(items: any) {
        console.log(items);
        this.tags=items

      }
 
      sendnewsletter(){
        (<HTMLInputElement> document.getElementById("sendnews")).disabled=true;

        console.log(this.selectedItems);
        var articleid=[]
        for(var i =0; i< this.articleItems.length ;i++){
          articleid.push(this.articleItems[i]['item_id']);
        }
        var email=[]
        console.log('emaillllllllllllllllllllllll',this.userItems)
        for(var j =0; j< this.userItems.length ;j++){
          email.push(this.userItems[j]['item_id']);
        }
        var postbody={
          articleid: articleid,
          email: email

        }
        this.expertservice.sendnewsletterbyadmin(postbody).subscribe((data:any)=>{
          console.log("send news letter successfully",data);
          (<HTMLInputElement> document.getElementById("sendnews")).disabled=false;

          },(error:any)=>{
            (<HTMLInputElement> document.getElementById("sendnews")).disabled=false;

          console.log(error);
        })
      }
}