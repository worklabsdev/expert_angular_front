import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { ChangePassword } from './change-password';
import { ExpertService } from '.././providers/expert.service';
import { RouterModule,Router } from '@angular/router';
import { GlobalService} from '.././providers/global.service';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-expert-login-header',
  templateUrl: './expert-login-header.component.html',
  styleUrls: ['./expert-login-header.component.css']
})
export class ExpertLoginHeaderComponent implements OnInit  {
  logged_name:String;
  logged_email:string;
  logged_id:any;
  cpForm : FormGroup;
  expertdata:any;
  expertimage:any;
  msg : Message[]=[];
  val: number =4;
  userCreds :ChangePassword[]=[];
  profileimageurl:any;
  profileimage:any;
  profileimageflag:Boolean =false;
  expertData :any;
  expertise:any=[];
  expertisearray:any[];
  changemade:Boolean =false;
  profile_summary:string;
  serial_number:string;
  phone_number:number;
  youtubelink:string;
  whoareyou:string[];
  address:string;

  country: any;
  countries: any[];
  filteredCountriesSingle: any[];
  filteredCountriesMultiple: any[];
  constructor(public expertservice:ExpertService,public router:Router,public globalservice:GlobalService,public messageService : MessageService) {
    this.profileimageurl = this.globalservice.serverip() + 'public/experts/'
    this.expertimage = this.globalservice.getExpertImage().subscribe(message=>{
      this.getexpertdata();
    })
  }



  ngOnInit() {
    // this.logged_name = localStorage.user_name;
    // this.logged_email = localStorage.user_email;
    this.logged_id = localStorage.user_id;
    this.getexpertdata();
    console.log('Logged in name',this.logged_name);

    this.cpForm = new FormGroup({
      oldpassword : new FormControl('',[Validators.required]),
      newpassword : new FormControl('',[Validators.required,Validators.minLength(7)])
    })
  }



  getexpertdata() {
    this.expertservice.getexpertuser(this.logged_id).subscribe((data:any)=>{
      var Dat =data.data;
      this.logged_name = Dat.name ? Dat.name :'';
      this.logged_email = Dat.email ? Dat.email : '';
      this.profileimage = data.data.profileImage ? data.data.profileImage : '' ;
      this.expertData =Dat;
      this.serial_number = Dat.serial_number ? Dat.serial_number :'';
      this.profile_summary = Dat.profile_summary ? Dat.profile_summary : '' ;
      this.youtubelink = Dat.youtubelink ? Dat.youtubelink : '';
      this.whoareyou = Dat.whoareyou ? Dat.whoareyou : '';
      this.phone_number = Dat.phone_number ? Dat.phone_number : '';
      this.countries = Dat.expertise ? Dat.expertise : '';
      this.address = Dat.address ? Dat.address : '';
      // console.log("inside expertdata",this.profileimage);
      if(this.profileimage) {
        this.profileimageflag =true;
      }
    },(error:any)=>{
      console.log(error);
    })
  }
  changepassword() {
    var input_string = this.cpForm.value ? this.cpForm.value : {};
    if(input_string) {
      var str_obj = {
        oldpassword: this.cpForm.value.oldpassword,
        newpassword :  this.cpForm.value.newpassword,
        email:  localStorage.user_email
      }
      this.expertservice.changepassword(str_obj).subscribe((data:any)=>{
        document.getElementById('closebtn').click();
        console.log("successfull changed password",data);
      },(error:any)=>{
        console.log(error);
      })

    }
    else {
      console.log("No value found");
      // alert("No value entered");
    }
  }
  logout() {
    localStorage.clear();
    this.router.navigate(['/']);
  }

  getexpertise(event){
    console.log(event.query);
    this.expertservice.getexpertise().subscribe((data:any)=>{
      console.log("this is the first log",data.data.length);
      this.expertisearray = data.data;
      //  for(var t=0;t<data.data.length;t++){
      // this.expertisearray.push(data.data[t]);
      //  }
      console.log("this is the data for expertise",this.expertisearray);
    },(error:any)=>{
      console.log(error);
    })
  }


  filterCountryMultiple(event) {
    let query = event.query;
    this.expertservice.getexpertise().subscribe(countries => {
      // console.log("this is countries",countries);
      this.filteredCountriesMultiple = this.filterCountry(query, countries.data);
    });
  }
  filterCountry(query, countries: any[]):any[] {
    this.changemade= true;
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    console.log("countries length",length);
    let filtered : any[] = [];
    for(let i = 0; i < countries.length; i++) {
      let country = countries[i];
      console.log("expertise ",country.expertise);
      if(country.expertise.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country);
      }
    }
    console.log("filtered",filtered);
    return filtered;
  }


  updateexpert() {
    console.log("this is who are you ",this.whoareyou);
    var data= {};

    if(this.changemade){
      var rat =[];
      if(this.countries && this.countries.length){
        for(var u=0;u<this.countries.length;u++){
          var exprt= {
            expertise:this.countries[u].expertise
          }
          rat.push(exprt);

          data = {
            name: this.logged_name,
            email: this.logged_email,
            profile_summary:this.profile_summary,
            serial_number:this.serial_number,
            expertise:rat,
            whoareyou:this.whoareyou,
            youtubelink:this.youtubelink,
            phone_number :this.phone_number,
            address: this.address
          }
          console.log("thisis rat",rat);
        }
        JSON.stringify(this.countries);
      }

      else {

        data = {
          name: this.logged_name,
          email: this.logged_email,
          profile_summary:this.profile_summary,
          serial_number:this.serial_number,
          whoareyou :this.whoareyou,
          youtubelink:this.youtubelink,
          phone_number :this.phone_number
        }
      }
      this.expertservice.updateexpert(this.logged_id,data).subscribe((data:any)=>{
        console.log("returned data",data);
        if(data.status==200) {
          document.getElementById('clsbtn').click();
          localStorage.setItem('user_name',data.data.name);
          localStorage.setItem('user_email',data.data.email);
          this.messageService.add({severity:'success', summary:'Updated successfully'});
        }
        console.log(data);
      },(error:any) =>{
        console.log(error);
      })
    }
  }



}
