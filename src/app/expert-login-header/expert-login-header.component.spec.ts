import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertLoginHeaderComponent } from './expert-login-header.component';

describe('ExpertLoginHeaderComponent', () => {
  let component: ExpertLoginHeaderComponent;
  let fixture: ComponentFixture<ExpertLoginHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertLoginHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertLoginHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
