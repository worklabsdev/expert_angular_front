export interface ChangePassword {
  email:string,
  oldpassword:string,
  newpassword:string
}
