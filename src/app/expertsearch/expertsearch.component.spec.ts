import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertsearchComponent } from './expertsearch.component';

describe('ExpertsearchComponent', () => {
  let component: ExpertsearchComponent;
  let fixture: ComponentFixture<ExpertsearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertsearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
