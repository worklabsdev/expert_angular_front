import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  logged_name:string;
  logged_email:string;
  logged_id:any;
  constructor() { }

  ngOnInit() {
    this.logged_name = localStorage.user_name;
    this.logged_email = localStorage.user_email;
    var logged_id = localStorage.user_id;
  }

}
