import { Injectable } from '@angular/core';
import { Http,HttpModule } from '@angular/http';
import { Observable  } from 'rxjs';
import { Subject } from 'rxjs';
// import  { Subject } from 'rxjs/Subject';
@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  private expertdata = new Subject<any>();
  private expertimage = new Subject<any>();
  private articlecat = new Subject<any>();
  constructor() { }

  serverip() {
   const serv = "http://localhost:3000/";
    //  const serv = "http://159.89.2.30:3000/";
    return serv;
  }
  authenticate() {
    // return this.http.post(this.globalservice)
  }
  back(){
    window.history.back();
  }

  updateexpert_service(message:String){
    this.expertdata.next({text:message});
  }
  getExpert(): Observable<any> {
    return this.expertdata.asObservable();
  }

  updateimage_service(message:String) {
    this.expertimage.next({text:message});
  }
  getExpertImage(): Observable<any> {
    return this.expertimage.asObservable();
  }

  updatearticles_category(message:String) {
    this.articlecat.next({text:message});
  }
  getArticleCat(): Observable<any> {
    return this.articlecat.asObservable();
  }
}
