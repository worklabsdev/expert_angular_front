import { Injectable } from '@angular/core';
import { HttpModule,Http } from '@angular/http';
import { GlobalService } from './global.service';
import 'rxjs';
import { map }  from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ExpertService {


  constructor(public http:Http,public globalservice : GlobalService) { }
  authenticate(credentials){
    return this.http.post(this.globalservice.serverip() + 'expert/authenticate',credentials)
    .pipe(map(res => res.json()));
  }
  checkemailpresence(email) {
    return this.http.get(this.globalservice.serverip() + 'expert/checkemailpresence/' + email)
    .pipe(map(res => res.json()));
  }
  checkphonepresence(phone_number) {
    return this.http.get(this.globalservice.serverip() + 'expert/checkphonepresence/' + phone_number)
    .pipe(map(res => res.json()));
  }
  register_user(userInfo) {
    return this.http.post(this.globalservice.serverip() + 'expert/register',userInfo)
    .pipe(map(res => res.json()));
  }
  changepassword(data) {
    return this.http.post(this.globalservice.serverip() + 'expert/changepassword',data)
    .pipe(map(res => res.json()));
  }
  forgotpassword(data){
    return this.http.post(this.globalservice.serverip() + 'expert/forgotpassword',data)
    .pipe(map(res => res.json()));
  }
  getfollowed(data){
    return this.http.post(this.globalservice.serverip() + 'expert/getfollowedlist',data)
    .pipe(map(res => res.json()));
  }
  getfollowers(data){
    return this.http.post(this.globalservice.serverip() + 'expert/getfollowerlist',data)
    .pipe(map(res => res.json()));
  }
  updateexpert(id,data) {
    return this.http.put(this.globalservice.serverip() + 'expert/updateexpert/'+id,data)
    .pipe(map(res => res.json()));
  }

  getexpertuser(id) {
    return this.http.get(this.globalservice.serverip() + 'expert/getexpertuser/'+id)
    .pipe(map(res => res.json()));
  }
  getaccountconfirm(confirmCode){
    return this.http.get(this.globalservice.serverip() + "expert/confirm-account/" + confirmCode)
    .pipe(map(res => res.json()));
  }
  searching(data){
    return this.http.post(this.globalservice.serverip() + "expert/searchsuggestion" ,data)
    .pipe(map(res => res.json()));
  }

  uploadprofileimage(id,data) {
    return this.http.put(this.globalservice.serverip()+'expert/uploadprofileimage/'+ id,data)
    .pipe(map(res=> res.json()));
  }
  getexpertdata(id) {
    return this.http.get(this.globalservice.serverip() + 'expert/getexpertdata/' + id)
    .pipe(map(res => res.json()));
  }
  //  social sigin in services

  checkgid(gid) {
    return this.http.get(this.globalservice.serverip() + 'expert/checkgid/' + gid)
    .pipe(map(res => res.json()));
  }
  savegoogleid(userdata) {
    return this.http.post(this.globalservice.serverip() + 'expert/savegoogleid',userdata)
    .pipe(map(res => res.json()));
  }


  checkfbid(fid) {
    return this.http.get(this.globalservice.serverip() + 'expert/checkfbid/' + fid)
    .pipe(map(res => res.json()));
  }
  savefacebookid(userdata) {
    return this.http.post(this.globalservice.serverip() + 'expert/savefacebookid',userdata)
    .pipe(map(res => res.json()));
  }

  savearticle(postdata) {
    return this.http.post(this.globalservice.serverip() + 'expert/savearticle',postdata)
    .pipe(map(res=> res.json()));
  }
  getexpertarticles(user_id) {
    return this.http.get(this.globalservice.serverip() + 'expert/getexpertarticles/' + user_id)
    .pipe(map(res => res.json()));
  }
  getallarticles(data) {
    return this.http.post(this.globalservice.serverip() + 'expert/getallarticles',data)
    .pipe(map(res => res.json()));
  }
  getdraftarticle(data){
    return this.http.post(this.globalservice.serverip() + 'expert/getdraftarticle',data)
    .pipe(map(res => res.json()));
  }
  getrecentarticles() {
    return this.http.get(this.globalservice.serverip() + 'expert/getrecentarticles')
    .pipe(map(res => res.json()));
  }
  getsinglearticle(articleId){
    return this.http.get(this.globalservice.serverip() + 'expert/getsinglearticle/' + articleId)
    .pipe(map(res => res.json()));
  }
  getsingleprivatearticle(articleId) {
    return this.http.get(this.globalservice.serverip()  + 'expert/getsingleprivatearticle/'  + articleId)
    .pipe(map(res => res.json()));
  }

  getrecentuserarticles() {
    return this.http.get(this.globalservice.serverip() + 'expert/getrecentuserarticles')
    .pipe(map(res => res.json()));
  }
  editarticle(id,data) {
    console.log("data in service",data);
    return this.http.put(this.globalservice.serverip() + 'expert/editarticle/'+ id, data)
    .pipe(map(res => res.json()));
  }
  editarticleforfeedback(id,data) {
    console.log("data in service",data);
    return this.http.put(this.globalservice.serverip() + 'expert/editarticlefeedback/'+ id, data)
    .pipe(map(res => res.json()));
  }
  uploaddegree(id,data) {
    console.log("datainsideservice",data);
    return this.http.put(this.globalservice.serverip() + 'expert/uploaddegree/'+id ,data)
    .pipe(map(res => res.json()));
  }
  sendnewsletter(id,email) {
    console.log("You are getting all the things",id,email);
    return this.http.get(this.globalservice.serverip() + 'expert/sendnewsletter/'+ id + '/' + email)
    .pipe(map(res => res.json()));
  }
  sendnewsletterbyadmin(data) {
    console.log("You are getting all the things",data);
    return this.http.post(this.globalservice.serverip() + 'expert/sendnewsletterbyadmin',data)
    .pipe(map(res => res.json()));
  }
  approvearticlebyadmin(id){
    return this.http.get(this.globalservice.serverip() + 'expert/approvearticlebyadmin/'+ id)
    .pipe(map(res=> res.json()));
  }
  approvearticle(id) {
    console.log("getting id",id);
    return this.http.get(this.globalservice.serverip() + 'expert/approvearticle/'+ id)
    .pipe(map(res=> res.json()));
  }
  approveexpert(id) {
    console.log("getting id",id);
    return this.http.get(this.globalservice.serverip() + 'expert/approveexpert/'+ id)
    .pipe(map(res=> res.json()));
  }
  getallexperts(data) {
    return this.http.post(this.globalservice.serverip() + 'expert/getallexperts',data)
    .pipe(map(res => res.json()));
  }
  getexpertise(){
    return this.http.get(this.globalservice.serverip() + 'expert/getallexpertise/')
    .pipe(map(res => res.json()));
  }
  gettag(){
    return this.http.get(this.globalservice.serverip() + 'expert/getalltags/')
    .pipe(map(res => res.json()));
  }
  getusers(data){
    console.log(data);
    return this.http.post(this.globalservice.serverip() + 'expert/userandarticle' , data)
    .pipe(map(res => res.json()));
  }
  getcategoryarticle(cat) {
    return this.http.get(this.globalservice.serverip() + 'expert/getcategoryarticle/'+ cat)
    .pipe(map(res => res.json()));
  }
  getnamearticle(name) {
    return this.http.get(this.globalservice.serverip() + 'expert/getnamearticle/'+ name)
    .pipe(map(res => res.json()));
  }
  saveVideoArticle(data) {
    return this.http.post(this.globalservice.serverip() + 'expert/savevideoarticle',data)
    .pipe(map(res => res.json()));
  }
  sendeditedarticle(articleid) {
    return this.http.get(this.globalservice.serverip() + 'expert/sendeditedarticle/'+ articleid)
    .pipe(map(res => res.json()));
  }
  get_en_ds_articles(enabled) {
    return this.http.get(this.globalservice.serverip()+ 'expert/getsinglearticlestatus/'+ enabled)
    .pipe(map(res => res.json()));
  }
  getpostcount() {
    return this.http.get(this.globalservice.serverip()+ 'expert/getpostcount')
    .pipe(map(res => res.json()));
  }
  getexpertcount() {
    return this.http.get(this.globalservice.serverip() + 'expert/getexpertcount')
    .pipe(map(res => res.json()));
  }
  updateprofileDescription(id,data) {
    return this.http.put(this.globalservice.serverip() + 'expert/updateprofileDescription/'+ id,data)
    .pipe(map(res => res.json()));
  }
  getexpertarticlesbyname(id) {
    return this.http.get(this.globalservice.serverip() + 'expert/exp/article/'+id)
    .pipe(map(res => res.json()));
  }

  // followexpert(followdata) {
  //   return this.http.post(this.globalservice.serverip() + 'expert/follow/expert',followdata)
  //   .pipe(map(res => res.json()));
  // }
  followexpert(postdata) {
    return this.http.post(this.globalservice.serverip() + 'expert/follow',postdata)
    .pipe(map(res => res.json()));

  }
  getfollowstatus(postdata){
    return this.http.post(this.globalservice.serverip() + 'expert/statusfollow',postdata)
    .pipe(map(res => res.json()));
  }
  unfollow(postdata) {
    return this.http.post(this.globalservice.serverip() + 'expert/unfollow',postdata)
    .pipe(map(res => res.json()));
  }

  getexpertrecentarticles(){
    return this.http.get(this.globalservice.serverip() + 'expert/getexpertrecentarticles')
    .pipe(map(res => res.json()));
  }
  LikeClick(likedata){
    return this.http.post(this.globalservice.serverip() + 'expert/favouriteadd',likedata)
    .pipe(map(res => res.json()));
  }

  UnlikeClick(unlikedata){
    return this.http.post(this.globalservice.serverip() + 'expert/favouriteremove',unlikedata)
    .pipe(map(res => res.json()));
  }
  commentClick(commentdata){
    return this.http.post(this.globalservice.serverip() + 'expert/comment',commentdata)
    .pipe(map(res => res.json()));
  }
  commentClickexpert(commentdata){
    return this.http.post(this.globalservice.serverip() + 'expert/commentprofile',commentdata)
    .pipe(map(res => res.json()));
  }
  getsearchedarticle(body){
    return this.http.post(this.globalservice.serverip() + 'expert/expertsearch', body )
    .pipe(map(res => res.json()));
  }
}
