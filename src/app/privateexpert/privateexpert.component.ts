import { Component, OnInit,EventEmitter } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { ExpertService } from '../providers/expert.service';
import { GlobalService } from '../providers/global.service';
import { Router,ActivatedRoute, Params } from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
import {RatingModule} from 'primeng/rating';
import { DomSanitizer  } from '@angular/platform-browser';
@Component({
  selector: 'app-privateexpert',
  templateUrl: './privateexpert.component.html',
  styleUrls: ['./privateexpert.component.css']
})
export class PrivateexpertComponent implements OnInit {
  logged_name:string;
  logged_email:string;
  logged_id :string;
  profilepercentage:any;
  profileimage:string;
  profile_summary:string;
  serial_number:string;
  expertise:any=[];
  imageformflag:Boolean =true;
  youtubelink:string;
  profile_description:any;
  profileimageflag:Boolean=false;
  profileimageurl:string;
  expertData:any;
  expertid:any;
  expertForm : FormGroup;
  profile_desc:any;

  constructor(private sanetizer:DomSanitizer,public expertservice : ExpertService,  private activatedRoute:ActivatedRoute,public globalservice: GlobalService,
      public messageService : MessageService,public router:Router) {
    this.profileimageurl = this.globalservice.serverip() + 'public/experts/';
    this.activatedRoute.params.subscribe((params)=>{
      this.expertid = params['expert_id'];
    })
  }

  ngOnInit() {
    this.expertForm =  new FormGroup({

      summary: new FormControl('',[Validators.required]),
    
    })
    this.logged_name = localStorage.user_name;
    this.logged_email = localStorage.user_email;
    this.logged_id = localStorage.user_id;
    this.getexpertuser();
    console.log(this.router.url,"Current URL");
  }
  getexpertuser() {
    this.expertservice.getexpertuser(this.expertid  ).subscribe((data:any)=>{
      if(data) {
        console.log("You got the data######",data);
        if(data.status==200) {
          var Dat = data.data;
          console.log("DAT expertise",Dat.expertise);
          console.log("percentage",data.data.profile_status_percentage);
          this.profilepercentage=Dat.profile_status_percentage;
          this.profileimage=Dat.profileImage;
          this.serial_number = Dat.serial_number;
          this.expertise = Dat.expertise;
          this.profile_summary = Dat.profile_summary;
          this.youtubelink = Dat.youtubelink;
          this.expertData = data.data;
          this.profile_description = this.sanetizer.bypassSecurityTrustHtml(Dat.profile_description);
          // this.countries = Dat.expertise;
          console.log(Dat.profile_description)
          this.profile_desc=Dat.profile_description
          console.log("This is profile image",this.profileimage);
          if(this.profileimage){
            this.profileimageflag =true;
            this.imageformflag  =false;
          }

        }
      }
    },(error:any)=>{
      console.log(error);
    })
  }



  saveexpert(id){
    // alert("ok")
    console.log(id)
    console.log(this.expertForm.value.summary)
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();

    // var newdate = day + "/" + month + "/" + year;
    var commentData=[];
    commentData.push({
      expert_id: id, 
      expert_comment: this.expertForm.value.summary,
      comment_date: day + "/" + month + "/" + year,
      user_id: localStorage.getItem('user_id'),
      user_name: localStorage.getItem('user_name'),
      user_email: localStorage.getItem('user_email')

  });
  console.log(commentData[0])
      this.expertservice.commentClickexpert(commentData[0]).subscribe((data:any)=>{
        // this.like = true;
        // this.likeCount=this.likeCount+1;
        console.log("successfully add comment",data)
        this.messageService.add({severity:'success', summary:'comment add successfully'});
        this.getexpertuser();
        this.expertForm.reset();
        },(error:any)=>{
        console.log("this is error",error);
      })
  }




}
