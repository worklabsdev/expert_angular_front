import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateexpertComponent } from './privateexpert.component';

describe('PrivateexpertComponent', () => {
  let component: PrivateexpertComponent;
  let fixture: ComponentFixture<PrivateexpertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateexpertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateexpertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
