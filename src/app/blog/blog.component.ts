import { Component, OnInit } from '@angular/core';
import { ExpertService  } from '../providers/expert.service';
import { GlobalService } from '../providers/global.service';
import { Router,ActivatedRoute,Params } from '@angular/router';
import { NgxMasonryOptions } from 'ngx-masonry';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  articleData:any;
  projectionimageurl:String;
  userarticleData:any;
  search:any;
  limit:number=0;
  expertarticleData:any;
  constructor(public expertservice : ExpertService,public router:Router,public globalservice:GlobalService) {
  this.projectionimageurl  = this.globalservice.serverip() + 'articles/projections/'
  }

  ngOnInit() {
    this.getallarticles();
    this.recentArticles();
    // this.getrecentuserarticles();
    this.getexpertrecentarticles();
    // this.getsearchedarticle();
  }
  experts(){
    if(this.search){
    localStorage.setItem('search',this.search)
    }
    this.router.navigate(['/experts'])

  }
  videos(){
    if(this.search){
    localStorage.setItem('search',this.search)

    }
    
    this.router.navigate(['/videos'])

  }
  getallarticles(){
    console.log(this.limit)
   
    var body={

        limit:this.limit+8,
        type:'article'
        
    }
    console.log(body)
    this.expertservice.getallarticles(body).subscribe((data:any)=>{
      console.log("You got the data",data);
      this.limit=this.limit+8

      this.articleData = data;
      if(localStorage.getItem('search')){
        this.searchName(localStorage.getItem('search'))
      }
    },(error:any)=>{
      console.log("You got error while getting articles",error);
    })
 }

 

 singlearticle(articleId) {
   if(articleId){
  this.router.navigate(['/article',articleId]);
   }
 }
 getexpertrecentarticles() {
   this.expertservice.getexpertrecentarticles().subscribe((data:any)=>{
     console.log('You got the user data ---  ',data);
     this.expertarticleData = data.data;
   },(error:any)=>{
     console.log("you got the error while getting user articles ",error);
   })
 }
 articlecat(category) {
  console.log("you got category",category);
  this.router.navigate(['/articlecat',category]);

  setTimeout(()=>{
  this.globalservice.updatearticles_category('category changed');
},500);
}


recentArticles(){
  this.expertservice.getrecentarticles().subscribe((data:any)=>{
    console.log("You got the data",data);
    this.userarticleData = data;
  },(error:any)=>{
    console.log("You got error while getting articles",error);
  })
}

articletag(tag) {
  console.log("you got category",tag);
  this.router.navigate(['/articlecat',tag]);

  setTimeout(()=>{

  for(var i=0; i<this.articleData.length; i++){
    // console.log(this.allArticleData[i])
    // this.articleData.push(this.allArticleData[i])
     for(var j=0; j<this.articleData[i]['article_tags'].length; j++){
        var datatag=this.articleData[i]['article_tags'][j]['item_text']
        if(tag==datatag){
          console.log('this.allArticleData[i]')
          this.articleData.push(this.articleData[i])
        }
     }
  }
  // this.globalservice.updatearticles_category('category changed');
},500);
}

searchName(event){
  console.log(event)
  // this.articleData=this.articleData;
  // console.log((<HTMLInputElement>document.getElementById("mySearch")).value)
  
  var input, filter, ul, li, a, i;
  if(!localStorage.getItem('search')){
    input = (<HTMLInputElement>document.getElementById("mySearch")).value;
  }
  if(localStorage.getItem('search')){
    input = event;
  }
  this.search=input;
  console.log("iiiiiiiiiiiiiiiiii",input)
  filter = input.toUpperCase();
  console.log(filter)

  var body =
    {
      "name" : filter,
      "searchin" : "article"
    }
  
      this.expertservice.getsearchedarticle(body).subscribe((data:any)=>{
        console.log("this is the first search",data);
        // this.body = data.data;
        //  for(var t=0;t<data.data.length;t++){
        // this.expertisearray.push(data.data[t]);
        //  }          
            this.articleData = data;

},)


  ul = document.getElementById("grid");
  console.log(ul)
  li = ul.getElementsByClassName("item");
  localStorage.removeItem('search')
  console.log('liiiiiiiiiiii',li)
  console.log(li.length)
  setTimeout(function() {
      for (i = 0; i < li.length; i++) {
      console.log('particular lli', li[i])
      
          a = li[i].getElementsByTagName("h3")[0];
          console.log(li[i].getElementsByTagName("h3")[0])
          if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            console.log('show',li[i])
              li[i].style.display = "";
          } else {
            console.log('hidden',li[i])

              li[i].style.display = "none";
          }
      }

  },100);
 


}
}
