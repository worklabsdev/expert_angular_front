import { Component, OnInit,EventEmitter } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { ExpertService } from '../providers/expert.service';
import { GlobalService } from '../providers/global.service';
import { Router,ActivatedRoute, Params } from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
import {RatingModule} from 'primeng/rating';
@Component({
  selector: 'app-expertinfobar',
  templateUrl: './expertinfobar.component.html',
  styleUrls: ['./expertinfobar.component.css']
})
export class ExpertinfobarComponent implements OnInit {
  logged_name:string;
  logged_email:string;
  logged_id :string;
  profilepercentage:any;
  profileimage:string;
  profile_summary:string;
  serial_number:string;
  expertise:any=[];
  imageformflag:Boolean =true;
  youtubelink:string;
  profile_description:any;
  profileimageflag:Boolean=false;
  profileimageurl:string;
  expertData:any;
  expertId:string;
  whoareyou:string[];
  countries: any[];
  follower:number=0;
  followed:number=0;
  abc:any;
  constructor(public expertservice : ExpertService,  private activatedRoute:ActivatedRoute,public globalservice: GlobalService,
    public messageService : MessageService,public router:Router) {
      this.profileimageurl = this.globalservice.serverip() + 'public/experts/';
      this.expertId = this.activatedRoute.snapshot.params.expert_id;
     
    }

    ngOnInit() {
      this.getexpertdata();
    }
    getexpertdata() {

      this.expertservice.getexpertuser(this.expertId).subscribe((data:any)=>{
        var me=this;
        var Dat =data.data;
        console.log(data)
        me.follower=Dat.follower.length;
        me.followed=Dat.followed.length;
        console.log(this.follower)
        this.logged_name = Dat.name ? Dat.name :'';
        // this.logged_email = Dat.email ? Dat.email : '';
        this.profileimage = data.data.profileImage ? data.data.profileImage : '' ;
        this.expertData =Dat;
        this.serial_number = Dat.serial_number ? Dat.serial_number :'';
        this.profile_summary = Dat.profile_summary ? Dat.profile_summary : '' ;
        this.youtubelink = Dat.youtubelink ? Dat.youtubelink : '';
        this.whoareyou = Dat.whoareyou ? Dat.whoareyou : '';
        // this.phone_number = Dat.phone_number ? Dat.phone_number : '';
        this.countries = Dat.expertise ? Dat.expertise : '';
        // this.address = Dat.address ? Dat.address : '';
        // console.log("inside expertdata",this.profileimage);
        setTimeout(()=>{
          this.globalservice.getExpert();
        },500);
        if(this.profileimage) {
          this.profileimageflag =true;
        }
      },(error:any)=>{
        console.log(error);
      })
    }
}
