import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertinfobarComponent } from './expertinfobar.component';

describe('ExpertinfobarComponent', () => {
  let component: ExpertinfobarComponent;
  let fixture: ComponentFixture<ExpertinfobarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertinfobarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertinfobarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
