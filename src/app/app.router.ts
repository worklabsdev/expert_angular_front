import  { ModuleWithProviders } from '@angular/core';
import  { RouterModule,Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateComponent } from './articles/create/create.component';
import { ListComponent } from './articles/list/list.component';
import { SingleComponent } from './articles/single/single.component';
import { SinglePrivateArticleComponent } from './articles/single-private-article/single-private-article.component';
import { EditArticleComponent } from './articles/edit-article/edit-article.component';
import { ProfileComponent } from './profile/profile.component';
import { BlogComponent } from './blog/blog.component';
import { AdminarticleapprovalComponent } from './adminarticleapproval/adminarticleapproval.component';
import { ExpertregistrationComponent } from './expertregistration/expertregistration.component';
import { ExpertarticlecatComponent } from './articles/expertarticlecat/expertarticlecat.component';
import { ExpertsearchComponent } from './expertsearch/expertsearch.component';
import { ExpertpublicComponent } from './expertpublic/expertpublic.component';
import { ExpertArticlesComponent } from './expert-articles/expert-articles.component';
import { ExpertsComponent } from './experts/experts.component';
import { PrivateexpertComponent } from './privateexpert/privateexpert.component';
import { AdminExpertComponent } from './admin/expert-profile/expert.component';
import { LoginConfirmComponent } from './login-confirm/login-confirm.component';
import { NewsLetterComponent } from './admin/newsletter/newsletter.component';
import { VideosComponent  } from './Videos/videos.component';
import { SingleVideoComponent } from './singlevideo/singlevideo.component';
import { FollowingsComponent } from './followings/followings.component';
import { AboutUsComponent } from './aboutUs/aboutUs.component';
import { VisionComponent } from './vision/vision.component';
import { FollowersComponent } from './followers/followers.component';
import { ArticlesDraftComponent } from './articles/draft/draft.component';


const appRoutes:Routes =[
{ path:'',component:HomeComponent },
{ path:'blog',component:BlogComponent },
{ path:'dashboard', component:DashboardComponent },
{ path : 'createarticle', component:CreateComponent },
{ path : 'articles', component:  ListComponent},
{ path : 'article/:articleId', component: SingleComponent },
{ path : 'singlearticle/:articleId', component:SinglePrivateArticleComponent },
{ path : 'editsinglearticle/:articleId', component: EditArticleComponent },
{ path : 'profile', component: ProfileComponent },
{ path : 'adminapproval/:articleId',component:AdminarticleapprovalComponent},
{ path : 'becomeexpert', component:ExpertregistrationComponent },
{ path : 'articlecat/:cat',component :ExpertarticlecatComponent },
{ path : 'expertsearch', component:ExpertsearchComponent },
{ path : 'expert/:expert_id',component:ExpertpublicComponent },
{ path : 'expertp/:expert_id', component:PrivateexpertComponent},
{ path : 'exp/articles/:expert_id', component: ExpertArticlesComponent},
{ path : 'experts', component:ExpertsComponent},
{ path : 'adminexpertapproval/:expert_id', component: AdminExpertComponent},
{ path : 'login/:confirmCode', component: LoginConfirmComponent},
{ path : 'admin/newsletter', component: NewsLetterComponent},
{ path : 'videos', component: VideosComponent},
{ path : 'singlevideos/:video_id', component: SingleVideoComponent },
{ path : 'followings', component: FollowingsComponent},
{ path : 'aboutus', component: AboutUsComponent },
{ path : 'ourvision', component: VisionComponent },
{ path : 'followers', component: FollowersComponent },
{ path : 'draft', component: ArticlesDraftComponent }


]

export const AppRouting = RouterModule.forRoot(appRoutes);
