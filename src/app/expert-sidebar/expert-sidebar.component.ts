import { Component, OnInit } from '@angular/core';
import { Router, RouterModule,ActivatedRoute } from '@angular/router';
import { ExpertService } from '../providers/expert.service';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { ChangePassword } from './change-password';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { ExpertinfobarComponent } from '../expertinfobar/expertinfobar.component';

@Component({
  selector: 'app-expert-sidebar',
  templateUrl: './expert-sidebar.component.html',
  styleUrls: ['./expert-sidebar.component.css'],
  providers:[ExpertinfobarComponent]
})
export class ExpertSidebarComponent implements OnInit {
  articleData :any;
  expert_loggedin_flag :number =1;
  unpublised_articles_counter:number = 0;
  expert_id:any;
  user_id:any;
  changePasswordForm : FormGroup;
  btn_actionloading:Boolean = false;
  emailpresentflag:Boolean =true;
  userCreds :ChangePassword[]=[];
  otherexpert:number =0;
  follow_status:number = 0;
  follow_process_loading:number =0;
  msg : Message[]=[];
  show_fs_logged_expert:number= 0;

  constructor(public ExpertinfobarComponent:ExpertinfobarComponent, public router:Router,public expertservice:ExpertService,public activatedRoute:ActivatedRoute, public messageService : MessageService) {
    this.activatedRoute.params.subscribe((params)=>{
      this.expert_id = params['expert_id'];
    })
  }

  ngOnInit() {
    this.user_id = localStorage.user_id;
    // if(user_id) {
    //   this.expert_loggedin_flag= 0;
    // }
    if(this.user_id != this.expert_id) {
      this.otherexpert =1;
    }
    console.log(this.router.url,"Current URL");
    var param =  this.router.url;
    var firstparams = param.split("/");
    console.log("tis is the first params",firstparams[1]);
    if(firstparams[1] =='expertp' || firstparams[1]=='exp') {
      this.show_fs_logged_expert = 1;
    }
    this.getexpertarticles(this.user_id);
    this.changePasswordForm = new FormGroup({
      email : new FormControl(localStorage.getItem('user_email')),
      oldpassword : new FormControl('',[Validators.required,Validators.minLength(7)]),
      newpassword : new FormControl('',[Validators.required,Validators.minLength(7)]),
      confirmnewpassword : new FormControl('',[Validators.required,Validators.minLength(7)])

    })
    this.getfollowstatus();
  }

  // checkemailpresence() {
  //   let email = this.changePasswordForm.value.email;
  
  //   if(email) {
  //     this.expertservice.checkemailpresence(email).subscribe((data:any)=>{
  //       console.log("data without data",data);
  //       if(data){
  //         console.log("This mail is data",data);
  //         this.emailpresentflag = true;
  //         // this.mess ageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
  //         if(this.changePasswordForm.value.email!=""){
  //         this.messageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
  //         }
  //         return false;
  //       }
  //     },(error:any)=>{
  //       if(this.changePasswordForm.value.email==""){
  //         this.messageService.add({severity:'error', summary:'Account Not Exist', detail:'Please try another email'});
  //         }
  //       this.emailpresentflag = false;

  //       console.log("Error");
  //     })
  //   }
  // }
  changePassword(){
    (<HTMLInputElement> document.getElementById("change")).disabled=true
    console.log(this.emailpresentflag)

    if(localStorage.getItem('user_email')) {
      // if(!this.phonepresentflag) {

    this.btn_actionloading=true;
    var input = this.changePasswordForm.value ? this.changePasswordForm.value : {};

    if(input){
console.log(input)
console.log(input.newpassword)
if(input.newpassword==input.confirmnewpassword){

      this.expertservice.changepassword(this.changePasswordForm.value).subscribe((data:any)=>{
        console.log(data)
        if(data) {
          // console.log(data)
          // document.getElementById('expertLoginModal').style.display='none';
         
          // document.getElementById('close_changepassword').click=true;
          this.btn_actionloading = false;
          this.messageService.add({severity:'success', summary:'Password changed Sucessfully'});
          setTimeout(function(){ 

          document.getElementById('close_changepassword').click();
        }, 1000);
       
        }
        // this.messageService.add({severity:'success', summary:'Password changed Sucessfully'});

      },(error:any)=>{
        this.btn_actionloading=false;
        setTimeout(()=>{
          this.messageService.add({severity:'warn', summary:'Invalid Credentials', detail:'Your Email Id or password is incorrect'});
          (<HTMLInputElement> document.getElementById("change")).disabled=false

        },2000);
      })
    }
      else{
        this.messageService.add({severity:'warn', summary:'Invalid Credentials', detail:"password doesn't match"});

      }
    }
      else {
        console.log("Please Enter value");
        (<HTMLInputElement> document.getElementById("change")).disabled=false
      }
 
  }
    else{
      this.messageService.add({severity:'warn', summary:'Invalid Email id', detail:"Your Email id doesn't Exist in any Account"});
      (<HTMLInputElement> document.getElementById("change")).disabled=false
}
  }

  getexpertarticles(user_id){
    this.expertservice.getexpertarticles(user_id).subscribe((data:any)=>{
      // console.log("You got the data",data[1].isEnabled);
      for(var t=0;t<data.length;t++){
        if(data[t].mailsentbyadmin ==true && data[t].isEnabled == false)
        this.unpublised_articles_counter++;
      }
      console.log("you are getting the counter ",this.unpublised_articles_counter);
    },(error:any)=>{
      console.log("You got error while getting articles",error);
    })
  }

  // getfollowstatus() {
  //   var postData = {
  //     follower : localStorage.user_id,
  //     expert:this.expert_id
  //   }
  //   this.expertservice.getfollowstatus(postData).subscribe((data:any)=>{
  //     if(data.data.length) {
  //       this.follow_status = 1;
  //       console.log("you are following him",data);
  //     }
  //     else {
  //       this.follow_status = 0;
  //       console.log("not following")
  //     }

  //   },(error:any)=>{
  //     console.log("this is the error",error);
  //   })
  // }

  getfollowstatus() {
    var postData = {
      // follower : localStorage.user_id,
      expert_id:this.expert_id
    }
    console.log(postData)
    this.expertservice.getfollowstatus(postData).subscribe((data:any)=>{
      if(data){
      if(data.data.follower.length){
      console.log('dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',data)
      for (var i=0; i<data.data.follower.length; i++){
      if(data.data.follower[i]['user_id']==localStorage.getItem('user_id')) {
        this.follow_status = 1;
        console.log("you are following him",data);
        this.ExpertinfobarComponent.getexpertdata()
        }
        else {
          this.follow_status = 0;
          this.ExpertinfobarComponent.getexpertdata()

          // this.follow_process_loading= 0;
        }

        }
        
      }
      else {
        this.follow_status = 0;
        this.ExpertinfobarComponent.getexpertdata()

        // this.follow_process_loading= 0;
      }
    }
    },(error:any)=>{
      console.log("You got error while getting articles",error);
    })
  }

  navigatepuclicarticles() {
    this.router.navigate(['/exp/articles',this.expert_id]);
  }
  navigateto_expert_description(){
    this.router.navigate(['/expertp',this.expert_id]);
  }
  navigate_to_video(){
    this.router.navigate(['/videos',this.expert_id]);
  }
  navigate_to_following(){
    this.router.navigate(['/following',this.expert_id]);
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/']);
  }

  follow(){
    this.follow_process_loading = 1;
    var expert = this.expert_id;
    var followerid = localStorage.user_id;
    var type = 'expert';
    var followData =  {
      expert_id:expert,
      user_id:followerid,
      type:type,
      user_email:localStorage.getItem('user_email')
    }
    this.expertservice.followexpert(followData).subscribe((data:any)=>{
      console.log("this is the data",data);
      if(data){
        this.follow_process_loading=0;
          this.messageService.add({severity:'success', summary:'Following'});
          this.getfollowstatus();
          
      }

    },(error:any)=>{
      this.follow_process_loading=0;
      console.log("this is the error",error);
    })
  }
  unfollow() {
      this.follow_process_loading = 1;
    var expert = this.expert_id;
    var followerid = localStorage.user_id;
    var type = 'expert';
    var unfollowData =  {
      expert_id:expert,
      user_id:followerid,
      type:type,
      user_email:localStorage.getItem('user_email')

    }
    console.log(unfollowData)

    this.expertservice.unfollow(unfollowData).subscribe((data:any)=>{
      console.log(data)
      if(data) {
        // alert("done");
          this.follow_process_loading=0;
          this.messageService.add({severity:'success', summary:'You Un-Followed this expert'});
          this.getfollowstatus();
      }
      console.log("this is the data",data);
    },(error:any)=>{
      this.follow_process_loading=0;

      console.log("this is the error",error);
    })
  }
}
