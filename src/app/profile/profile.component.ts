import { Component, OnInit,EventEmitter } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { ExpertService } from '../providers/expert.service';
import { GlobalService } from '../providers/global.service';
import { Router,ActivatedRoute, Params } from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
import {RatingModule} from 'primeng/rating';
import { DomSanitizer,SafeResourceUrl,SafeUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  logged_name:string;
  logged_email:string;
  logged_id :string;
  profile_summary:string;
  serial_number:string;
  expertise:any=[];
  val: number =4;
  msg:Message[]=[];
  // main=[];
  count:number=0
  countedit:number=0;
  // expertise:string;
  events:any;
  eventsar: any[] = [];
  eventarray=[];
  profileimageflag:Boolean=false;
  imageformflag:Boolean =true;
  imageloadingflag : Boolean =false;
  profileimageurl:string;
  profile_64textString:string;
  degree_64textString:string;
  profileimage:string;
  profileimageloader:any='';
  changemade:Boolean=false;
  profilepercentage=10;
  youtubelink:string;
  expertisearray:any[];
  complete_expert_array=[];
  profile_description:any;
  expertData:any;
  whoareyou:any;
  phone_number:number;
  address:any;
  country: any;
  Degree=[];
  updateData:boolean;

  countries: any[];

  filteredCountriesSingle: any[];

  filteredCountriesMultiple: any[];
  degreeImageUrl:any;

  constructor(    public sanitizer: DomSanitizer,    private activatedRoute:ActivatedRoute,public expertservice:ExpertService,public globalservice: GlobalService,
    public messageService : MessageService,public router:Router) {
      this.profileimageurl = this.globalservice.serverip() + 'public/experts/'
      this.degreeImageUrl  = this.globalservice.serverip() + 'public/experts/degrees/'
      this.eventsar = [{
        degree_name:'',
        degree_image_path:'',
      }]

    }

    ngOnInit() {
      (<HTMLInputElement> document.getElementById("name")).style.display='block';
      // (<HTMLInputElement> document.getElementById("submit_button")).style.display="block";

      this.logged_name = localStorage.user_name;
      this.logged_email = localStorage.user_email;
      this.logged_id = localStorage.user_id;
      this.getexpertuser();
      this.getexpertdata();
    }
    getexpertise(event){
      console.log(event.query);
      this.expertservice.getexpertise().subscribe((data:any)=>{
        console.log("this is the first log",data.data.length);
        this.expertisearray = data.data;
        //  for(var t=0;t<data.data.length;t++){
        // this.expertisearray.push(data.data[t]);
        //  }
        console.log("this is the data for expertise",this.expertisearray);
      },(error:any)=>{
        console.log(error);
      })
    }
    filterCountryMultiple(event) {
      let query = event.query;
      this.expertservice.getexpertise().subscribe(countries => {
        // console.log("this is countries",countries);
        this.filteredCountriesMultiple = this.filterCountry(query, countries.data);
      });
    }
    filterCountry(query, countries: any[]):any[] {
      this.changemade= true;
      //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
      console.log("countries length",length);
      let filtered : any[] = [];
      for(let i = 0; i < countries.length; i++) {
        let country = countries[i];
        console.log("expertise ",country.expertise);
        if(country.expertise.toLowerCase().indexOf(query.toLowerCase()) == 0) {
          filtered.push(country);
        }
      }
      console.log("filtered",filtered);
      return filtered;
    }
    // updateexpert() {
    //   console.log("this is countries",this.countries);
    //   var rat =[];
    //   for(var u=0;u<this.countries.length;u++){
    //     var exprt= {
    //       expertise:this.countries[u].expertise
    //     }
    //     rat.push(exprt);
    //     console.log("thisis rat",rat);
    //   }
    //   JSON.stringify(this.countries);
    //
    //   if(this.changemade){
    //     var data = {
    //       name: this.logged_name,
    //       email: this.logged_email,
    //       profile_summary:this.profile_summary,
    //       serial_number:this.serial_number,
    //       expertise:rat,
    //       youtubelink:this.youtubelink
    //     }
    //     this.expertservice.updateexpert(this.logged_id,data).subscribe((data:any)=>{
    //       console.log("returned data",data);
    //       if(data.status==200) {
    //         this.globalservice.updateexpert_service('change expert allover');
    //         localStorage.setItem('user_name',data.data.name);
    //         localStorage.setItem('user_email',data.data.email);
    //         this.messageService.add({severity:'success', summary:'Updated successfully'});
    //       }
    //       console.log(data);
    //     },(error:any) =>{
    //       console.log(error);
    //     })
    //   }
    // }
    add_expertise_toarray(){
      this.changemade=true;
      this.complete_expert_array.push(this.expertise);
      console.log('you got the array complete ', this.complete_expert_array);
    }


    scrolltouploaddocuments() {
      window.scrollTo(0, 1200);
    }
    getexpertuser() {
      if(this.updateData){
        this.profilepercentage=10;
          }
      this.expertservice.getexpertuser(this.logged_id).subscribe((data:any)=>{
        if(data) {
          console.log("You got the data######",data);
          if(data.data) {
            var Dat = data.data;
            console.log("DAT expertise",Dat.expertise);
            console.log("percentage",data.data.profile_status_percentage);
            // this.profilepercentage=this.profilepercentage + Dat.profile_status_percentage;
            this.profileimage=Dat.profileImage;
            this.expertData = data.data;
            this.serial_number = Dat.serial_number;
            this.expertise = Dat.expertise;
            this.profile_summary = Dat.profile_summary;
            this.youtubelink = Dat.youtubelink;
            this.profile_description = Dat.profile_description;
            this.countries = Dat.expertise;
            console.log(this.expertData)
            console.log("This is profile image",this.youtubelink);
            if(this.profileimage){
              this.profileimageflag =true;
              this.imageformflag  =false;
            }

          }
        }
      },(error:any)=>{
        console.log(error);
      })
    }
    showimageuploading_input() {
      this.imageformflag=true;
      // this.profileimageflag=false;
      this.profile_64textString='';
    }
    //  =================================  profile image uploading code ==================
    handleFileSelect(evt){
      (<HTMLInputElement> document.getElementById("upload")).disabled=true;

      var files = evt.target.files;
      var file = files[0];
      if (files && file) {
        var reader = new FileReader();
        reader.onload =this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
        setTimeout(()=>{ 
        this.uploadprofileimage();
        }, 3000);
      }
    }

    _handleReaderLoaded(readerEvt) {
      var binaryString = readerEvt.target.result;
      this.profile_64textString= btoa(binaryString);
      this.profile_64textString = 'data:image/png;base64,' +this.profile_64textString;
      // console.log(btoa(binaryString));
    }

    uploadprofileimage() {
      this.profileimageloader=1;
      var data  = {
        profileimage  :this.profile_64textString
      }
      if(this.profile_64textString){
        this.expertservice.uploadprofileimage(this.logged_id,data).subscribe((data:any)=>{
          if(data.status==200){
            this.profileimageloader='';
            this.globalservice.updateimage_service('change expert images');
            this.messageService.add({severity:'success', summary:'Profile Picture Changed'});
            this.updateData=true;
            this.getexpertuser();
            this.getexpertdata()

          }
        },(error:any)=>{
          (<HTMLInputElement> document.getElementById("upload")).disabled=true;

          console.log(error);
        })
      }
    }
    // ========================================================================================
    // ===================================== Degree image uploading code ======================

         handledegreeFileSelect(evt) {
       var files = evt.target.files;
       console.log("getting files",files);
       var file = files[0];
       if(files && file) {
         var reader = new FileReader();
         reader.onload = this._handledegreeReaderLoaded.bind(this);
         reader.readAsBinaryString(file);
       }
     }
     _handledegreeReaderLoaded(readerEvt) {
       console.log("target",readerEvt)
       var binaryString= readerEvt.target.result;
       this.degree_64textString = btoa(binaryString);
       this.degree_64textString = 'data:image/png;base64,' +this.degree_64textString;
       console.log(this.eventsar)
       console.log(this.eventsar.length)
      //  this.main[this.count]=this.degree_64textString

        // for(var k=0;k<this.eventsar.length;k++){
         this.eventsar[this.count].degree_image_path= this.degree_64textString;
         
        //  this.main[this.count].push(this.degree_64textString)

        //  }
       // console.log("in handledegreereader",this.eventsar);
     }
    // handledegreeFileSelect(evt) {
    //   var files = evt.target.files;
    //   console.log("getting files",evt);
    //   var file = files[0];
    //   if(files && file) {
    //     var reader = new FileReader();
    //     reader.onload = this._handledegreeReaderLoaded.bind(this);
    //     reader.readAsBinaryString(file);
    //   }
    // }
    // _handledegreeReaderLoaded(readerEvt) {
    //   console.log("target",readerEvt)
    //   var binaryString= readerEvt.target.result;
    //   this.degree_64textString = btoa(binaryString);
    //   this.degree_64textString = 'data:image/png;base64,' +this.degree_64textString;
    //   for(var k=0;k<this.eventsar.length;k++){
    //     this.eventsar[k].degree_image_path= this.degree_64textString;
    //   }
    //   // console.log("in handledegreereader",this.eventsar);
    // }

    // ========================================================================================
    addonedegree(){
      this.count=this.count+1
      console.log(this.eventsar)
      this.eventsar.push({});//push empty object of type user
      console.log(JSON.stringify(this.eventsar));
      // this.eventarray.push(this.events);
    }

    uploaddegree() {
      (<HTMLInputElement> document.getElementById("updegree")).disabled=true
      console.log(this.eventsar)
      console.log(typeof this.eventsar)


      this.count=0
      // console.log(this.main)
      console.log("this is the eventsar lenght",this.degree_64textString);
      // this.eventsar = JSON.stringify(this.eventsar);
      console.log("degreedata",this.eventsar.length);
      // for(var p =0;p<this.eventsar.length;p++){
      //     // this.eventsar[p].degree_image_path = '';
      //     this.eventsar[p].degree_image_path=this.degree_64textString[p];
      // }
      // fileChangeEvent(fileInput: any){
      //         this.filesToUpload = <Array<File>> fileInput.target.files;
      //     }

      var degreejson = {
        // degreename:this.eventsar,
        degree:this.eventsar
      }
      console.log("degree JSON",degreejson);
           this.expertservice.uploaddegree(this.logged_id,degreejson).subscribe((data:any)=>{

      // this.expertservice.uploaddegree(this.logged_id,degreejson.degree).subscribe((data:any)=>{
        if(data) {

          console.log("degree uploaded",data);
          this.updateData=true;
          this.getexpertuser();
          this.getexpertdata();
          (<HTMLInputElement> document.getElementById("updegree")).disabled=false
          this.messageService.add({severity:'success', summary:'Degree updated Successfully'});
             
          }
      },(error:any)=>{
        (<HTMLInputElement> document.getElementById("updegree")).disabled=false
this.count=0
        console.log(error);
      })
    }
    getexpertdata() {
      // alert("alert1"+this.updateData);
      if(this.updateData){
        // alert("alert2"+this.updateData);

      // this.logged_name='';
      // this.logged_email='';
      // this.profileimage='';
      // this.serial_number='';
      // this.profile_summary='';
      // this.youtubelink='';
      // this.whoareyou='';
      // this.countries.length=0;
      // this.address='';
      // this.profilepercentage=10;
      // this.phone_number;

      this.Degree.length=0;
      this.updateData=false;
    }
      this.expertservice.getexpertuser(this.logged_id).subscribe((data:any)=>{
        console.log(data)
        var Dat =data.data;
        this.logged_name = Dat.name ? Dat.name :'';
        this.logged_email = Dat.email ? Dat.email : '';
        this.profileimage = data.data.profileImage ? data.data.profileImage : '' ;
        this.expertData =Dat;
        this.serial_number = Dat.serial_number ? Dat.serial_number :'';
        this.profile_summary = Dat.profile_summary ? Dat.profile_summary : '' ;
        this.youtubelink = Dat.youtubelink ? Dat.youtubelink : '';
        this.whoareyou = Dat.whoareyou ? Dat.whoareyou : '';
        this.phone_number = Dat.phone_number ? Dat.phone_number : '';
        this.countries = Dat.expertise ? Dat.expertise : '';
        this.address = Dat.address ? Dat.address : '';
console.log(this.expertData)
       // Add by Akash for showing Degree Name

        for (var i=0; i<Dat.degree.length; i++){
          if(i<Dat.degree.length-1){
          this.Degree.push(Dat.degree[i]['degree_name']);
          this.Degree.push(",");
          }
          else{
            this.Degree.push(Dat.degree[i]['degree_name']);
            this.Degree.push(".");
            }
        }
        if(Dat.degree.length==0){
          this.Degree.push("Add Your Degree.");
        }
       
       // Add by Akash for increase 20% profile status percentage
        
        if(this.serial_number.length){
          this.profilepercentage=this.profilepercentage+2;
        }if(this.whoareyou.length){
          this.profilepercentage=this.profilepercentage+2;
        }if(this.phone_number.toString().length){
          this.profilepercentage=this.profilepercentage+2;
        }if(this.address.length){
          this.profilepercentage=this.profilepercentage+2;
        }if(this.profile_summary.length){
          this.profilepercentage=this.profilepercentage+10;
        }if(this.expertData.profile_description!=null){
          this.profilepercentage=this.profilepercentage+20;
        }if(Dat.degree.length){
          this.profilepercentage=this.profilepercentage+30;
        }
        if(Dat.profileImage.length){
          this.profilepercentage=this.profilepercentage+20;
        }if(Dat.whoareyou.length){
          this.profilepercentage=this.profilepercentage+2;
        }
        if(this.profilepercentage==100){
          // this.profilepercentage=100;
          (<HTMLInputElement> document.getElementById("submit_button")).style.display="block";
          // this.expertData.isEnabled=true;
         }
        if(this.profileimage) {
          this.profileimageflag =true;
        }
       if(!this.countries.length){
this.countries.push("No Expertise Added")
       }
       (<HTMLInputElement> document.getElementById("upload")).disabled=false;

      },(error:any)=>{
        (<HTMLInputElement> document.getElementById("upload")).disabled=false;

        console.log(error);
      })
    }
    updateprofileDescription() {
      // if(this.profile_64textString){
      var profiledata =  {
        profile_description: this.profile_description
      }
      console.log(profiledata)
      this.expertservice.updateprofileDescription(this.logged_id,profiledata).subscribe((data:any)=>{
        if(data.status==200){
          // this.profileimageloader='';
          // this.globalservice.updateimage_service('change expert images');
          this.updateData=true;
            this.getexpertuser();
            this.getexpertdata();
          this.messageService.add({severity:'success', summary:'Profile Description updated Successfully'});
          // this.getexpertuser();
        }
      },(error:any)=>{
        console.log(error);
      })
      // }
    }


    updateexpert() {
      (<HTMLInputElement> document.getElementById("youtube")).disabled=true;

      console.log('cccccccccccccccccccccccccccccccccccc',this.countedit)
      if(this.countedit==6){
      console.log("this is who are you ",this.whoareyou);
      var data= {};
console.log(this.countries)
      if(this.changemade){
        var rat =[];
        if(this.countries && this.countries.length){
          for(var u=0;u<this.countries.length;u++){
            var exprt= {
              expertise:this.countries[u].expertise
            }
            rat.push(exprt);

            data = {
              name: this.logged_name,
              email: this.logged_email,
              profile_summary:this.profile_summary,
              serial_number:this.serial_number,
              expertise:rat,
              whoareyou:this.whoareyou,
              youtubelink:this.youtubelink,
              phone_number :this.phone_number,
              address: this.address
            }
            console.log("thisis rat",rat);
          }
          JSON.stringify(this.countries);
        }

        else {

          data = {
            name: this.logged_name,
            email: this.logged_email,
            profile_summary:this.profile_summary,
            serial_number:this.serial_number,
            whoareyou :this.whoareyou,
            youtubelink:this.youtubelink,
            phone_number :this.phone_number,
            address :this.address,
            expertise :this.countries
          }
        }
        console.log("export Data To Be Upload",data)
        this.expertservice.updateexpert(this.logged_id,data).subscribe((data:any)=>{
          console.log("returned data",data);
          if(data) {
            console.log("abcdef")
            this.countedit=0;

            document.getElementById('clsbtn').click();
            localStorage.setItem('user_name',data.data.name);
            localStorage.setItem('user_email',data.data.email);
            this.updateData=true;
            this.getexpertuser();
            this.getexpertdata();
            this.messageService.add({severity:'success', summary:'Updated successfully'});
            // this.profilepercentage=this.profilepercentage + data.data.
          }
          // document.getElementById('clsbtn').click();

          // (<HTMLInputElement> document.getElementById("youtube")).disabled=false;

          console.log(data);
        },(error:any) =>{
          (<HTMLInputElement> document.getElementById("youtube")).disabled=false;

          console.log(error);
        })
      }
    }
    }

    submit_profile_for_review(){
      (<HTMLInputElement> document.getElementById("youtube")).disabled=true;

       var data={
        //  isEnabled:false,
         profile_status_percentage:100
        }
        this.expertservice.updateexpert(this.logged_id,data).subscribe((data:any)=>{
          console.log("returned data",data);
          if(data.status==200) {
            document.getElementById('clsbtn').click();
            localStorage.setItem('user_name',data.data.name);
            localStorage.setItem('user_email',data.data.email);
            this.updateData=true;
            this.getexpertuser();
            this.getexpertdata();
            this.messageService.add({severity:'success', summary:'Updated successfully'});
            // this.profilepercentage=this.profilepercentage + data.data.
            (<HTMLInputElement> document.getElementById("youtube")).disabled=false;

          }
          console.log(data);
        },(error:any) =>{
          console.log(error);
        })
   
    }
    next(){
 //classList.add("mystyle")
      this.countedit=this.countedit+1
      if(this.countedit==1){
      (<HTMLInputElement> document.getElementById("name")).style.display='none';
      (<HTMLInputElement> document.getElementById("addr")).style.display='block';    
    } else   if(this.countedit==2){
        (<HTMLInputElement> document.getElementById("addr")).style.display='none';
        (<HTMLInputElement> document.getElementById("summary")).style.display='block';
        }
        else   if(this.countedit==3){
          (<HTMLInputElement> document.getElementById("summary")).style.display='none';
          (<HTMLInputElement> document.getElementById("expertise")).style.display='block';
          }
          else   if(this.countedit==4){
            (<HTMLInputElement> document.getElementById("expertise")).style.display='none';
            (<HTMLInputElement> document.getElementById("wru")).style.display='block';
            } else   if(this.countedit==5){
              (<HTMLInputElement> document.getElementById("wru")).style.display='none';
              (<HTMLInputElement> document.getElementById("sno")).style.display='block';
              } else   if(this.countedit==6){
                (<HTMLInputElement> document.getElementById("sno")).style.display='none';
                (<HTMLInputElement> document.getElementById("youtube")).style.display='block';
              }
              else if(this.countedit==7){
                document.getElementById('clsbtn').click();

              }
    }
  }
