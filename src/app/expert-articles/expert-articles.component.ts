import { Component, OnInit,EventEmitter } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { ExpertService } from '../providers/expert.service';
import { GlobalService } from '../providers/global.service';
import { Router,ActivatedRoute, Params } from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-expert-articles',
  templateUrl: './expert-articles.component.html',
  styleUrls: ['./expert-articles.component.css']
})
export class ExpertArticlesComponent implements OnInit {
expert_id:any;
articleData:any = [];
projectionimageurl:any;
p: number = 1;
userid :any;
  constructor(public expertservice :ExpertService,public activatedRoute : ActivatedRoute,public globalservice:GlobalService,public router:Router) {
    this.expert_id = this.activatedRoute.snapshot.params.expert_id;
      this.projectionimageurl  = this.globalservice.serverip() + 'articles/projections/'
      this.getexpertarticlesbyname();
  }

  ngOnInit() {
    this.userid = localStorage.user_id;
    this.getexpertarticlesbyname();
  }


  getexpertarticlesbyname(){
    this.expertservice.getexpertarticlesbyname(this.expert_id).subscribe((data:any)=>{
      console.log("there is data",data);
      this.articleData = data.data;
    },(error:any)=>{
      console.log("there is error",error);
    })
  }

navigatesinglearticle(id) {
  this.router.navigate(['/article',id]);
}





}
