import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/providers/global.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(public globalservice: GlobalService,public router:Router) { }

  ngOnInit() {
  }
  articlecat(category) {
    console.log("you got category",category);
    this.router.navigate(['/articlecat',category]);

    setTimeout(()=>{
    this.globalservice.updatearticles_category('category changed');
  },500);
}
}
