import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { ExpertInterface } from './expertInterface';
import { ExpertService } from '../../../providers/expert.service';
import { GlobalService } from '../../../providers/global.service';
import { RouterModule,Router }  from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';


import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  expertList :ExpertInterface[]=[];
  loginForm : FormGroup;
  changePasswordForm:FormGroup;
  forgotPasswordForm:FormGroup;
  regForm : FormGroup;
  msgss : Message[]=[];
  sub:any;
  btn_actionloading:Boolean = false;
  emailpresentflag:Boolean =true;
  registerloading:Boolean =false;
  logged_id :any=0;
  logged_name:string;
 
  phonepresentflag:Boolean =true;
  constructor(public globalservice: GlobalService,public expertservice:ExpertService,private socialAuthService: AuthService,public router:Router,public messageServ : MessageService) { }

  ngOnInit() {
    if(localStorage.user_id){
    this.logged_id = localStorage.user_id;
    console.log("this is the localStorage",localStorage.user_id);
    this.getexpertdata();
    }



    this.loginForm = new FormGroup({
      email : new FormControl('',[Validators.required,Validators.email]),
      password : new FormControl('',[Validators.required,Validators.minLength(7)])
    })
    this.regForm = new FormGroup({
      name: new FormControl('',Validators.required),
      email :new FormControl('',[Validators.required,Validators.email]),
      password : new FormControl('',[Validators.required,Validators.minLength(7)]),
    })
    this.changePasswordForm = new FormGroup({
      email : new FormControl('',[Validators.required,Validators.email]),
      oldpassword : new FormControl('',[Validators.required,Validators.minLength(7)]),
      newpassword : new FormControl('',[Validators.required,Validators.minLength(7)]),
      confirmnewpassword : new FormControl('',[Validators.required,Validators.minLength(7)])

    })
    this.forgotPasswordForm = new FormGroup({
      email : new FormControl('',[Validators.required,Validators.email]),
    
    })
  }


  authenticate() {
    (<HTMLInputElement> document.getElementById("login")).disabled=true

    this.btn_actionloading=true;
    var input = this.loginForm.value ? this.loginForm.value : {};

    if(input){
console.log(input)
console.log(input.password)

      this.expertservice.authenticate(this.loginForm.value).subscribe((data:any)=>{
        if(data.token) {
          console.log(data)
          this.btn_actionloading = false;
          localStorage.setItem('token',data.token);
          localStorage.setItem('user_name',data.userdata.name);
          localStorage.setItem('user_email',data.userdata.email);
          localStorage.setItem('user_id',data.userdata._id);
          console.log("this is the loc",localStorage.followedexpert);
          if(localStorage.followedexpert) {
//             this.router.navigateByUrl('/header', {skipLocationChange: true}).then(()=>
// this.router.navigate(["Your actualComponent"]));
            this.router.navigate(['/expertp',localStorage.followedexpert]);
              document.getElementById('close_login').click();
              (<HTMLInputElement> document.getElementById("login")).disabled=false


          }
          else {
            console.log(localStorage.user_id);
            document.getElementById('close_login').click();
            this.router.navigate(['/articles']);
            (<HTMLInputElement> document.getElementById("login")).disabled=false

          }

        }
        if(data.status == 401){
          // alert('you here ');
          this.loginForm.value.username = '';
          this.loginForm.value.password =  '';
          this.btn_actionloading=false;
          this.messageServ.add({severity:'warn', summary:'Invalid Credentials', detail:'Your Email Id or password is incorrect'});
          (<HTMLInputElement> document.getElementById("login")).disabled=false

        }
        else  if(data.status == 402){
          // alert('you here ');
          this.loginForm.value.username = '';
          this.loginForm.value.password =  '';
          this.btn_actionloading=false;
          this.messageServ.add({severity:'warn', summary:'Please verify Account from Email', detail:'Please confirm your account through mail'});
          (<HTMLInputElement> document.getElementById("login")).disabled=false

        }
      },(error:any)=>{
        // if(error){
        this.loginForm.value.username = '';
        this.loginForm.value.password =  '';
        this.btn_actionloading=false;
        setTimeout(()=>{
          this.messageServ.add({severity:'warn', summary:'Invalid Credentials', detail:'Your Email Id or password is incorrect'});
          (<HTMLInputElement> document.getElementById("login")).disabled=false

        },2000);
        // }
      })
    }
    else {
      console.log("Please Enter value");
      (<HTMLInputElement> document.getElementById("login")).disabled=false

    }
  }
  articlecat(category) {
    console.log("you got category",category);
    this.router.navigate(['/articlecat',category]);

    setTimeout(()=>{
    this.globalservice.updatearticles_category('category changed');
  },500);


  }
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      console.log("PROVIDER_ID",GoogleLoginProvider.PROVIDER_ID);
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log("This is user data" ,userData);
        if(userData.provider=='google'){
          this.expertservice.checkgid(userData.id).subscribe((gdata:any)=>{

            if(gdata) {
              console.log("This is gid",gdata);
              localStorage.setItem('user_name',gdata.name);
              localStorage.setItem('user_email',gdata.email);
              localStorage.setItem('user_id',gdata._id);
              console.log("username"+ localStorage.user_name + "email" + localStorage.email);
              document.getElementById('close_main').click();
              this.router.navigate(['dashboard']);
            }
          },(gerror:any)=>{
            console.log("Thats gerror",gerror);
            if(gerror.status==400) {
              this.expertservice.savegoogleid(userData).subscribe((data:any)=>{
                if(userData.token) {
                  localStorage.setItem('token',userData.token);
                  localStorage.setItem('user_name',userData.name);
                  localStorage.setItem('user_email',userData.email);
                  localStorage.setItem('user_id',data._id);
                  document.getElementById('close_main').click();
                  this.router.navigate(['/dashboard']);
                }
              },(error:any)=>{
                console.log("Got error saving the google id",error);
              })
            }
          })
        }
        else if(userData.provider=='facebook') {
          this.expertservice.checkfbid(userData.id).subscribe((fbdata:any)=>{
            if(fbdata.status == 200) {
              console.log("This is fid",fbdata);
              localStorage.setItem('user_name',fbdata.name);
              localStorage.setItem('user_email',fbdata.email);
              localStorage.setItem('user_id',fbdata._id);
              console.log("username"+ localStorage.user_name + "email" + localStorage.email);
              document.getElementById('close_main').click();
              this.router.navigate(['dashboard']);
            }
          },(fberror:any)=>{
            this.expertservice.savefacebookid(userData).subscribe((data:any)=>{
              if(userData.token) {
                localStorage.setItem('token',userData.token);
                localStorage.setItem('user_name',userData.name);
                localStorage.setItem('user_email',userData.email);
                localStorage.setItem('user_id',data._id);
                document.getElementById('close_main').click();
                this.router.navigate(['/dashboard']);
              }
            },(error:any)=>{
              console.log("Got error saving the google id",error);
            })
          })
        }

      }
    );
  }


  // checkemailpresence() {
  //   console.log(this.regForm.value.email)
  //   console.log(this.loginForm.value.email)

  //   let email = this.regForm.value.email;
  //   if(email==""){
  //      email = this.changePasswordForm.value.email;
  //   }else if(email==""){
  //     email = this.forgotPasswordForm.value.email;

  //   }
  //   console.log('llllllllllllllllllllll',email)
  //   // let email = localStorage.getItem('user_email')
  //   if(email) {
  //     this.expertservice.checkemailpresence(email).subscribe((data:any)=>{
  //       console.log("data without data",data);
  //       if(data.status==200){
  //         this.emailpresentflag=true;
  //         return false;

  //       }
  //       else  {
  //         console.log("This mail is data",data);
  //         this.emailpresentflag = false;
  //         // this.mess ageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
  //         // this.messageServ.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
  //       }
  //     },(error:any)=>{
  //       // this.emailpresentflag = true;

  //       console.log("Error",error);
  //     })
  //   }
  // }
  checkemailpresence() {
    let email = this.regForm.value.email;
    if(email==""){
       email = this.changePasswordForm.value.email;
    }else if(email==""){
      email = this.forgotPasswordForm.value.email;

    }
    if(email) {
      this.expertservice.checkemailpresence(email).subscribe((data:any)=>{
        console.log("data without data",data);
        if(data){
          console.log("This mail is data",data);
          this.emailpresentflag = true;
          // this.mess ageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
          if(this.regForm.value.email!=""){
          this.messageServ.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
          }
          return false;
        }
      },(error:any)=>{
        if(this.regForm.value.email==""){
          this.messageServ.add({severity:'error', summary:'Account Not Exist', detail:'Please try another email'});
          }
        this.emailpresentflag = false;

        console.log("Error");
      })
    }
  }

  checkphonepresence() {
    let Phone_number = this.regForm.value.Phone_number;
    if(Phone_number) {
      this.expertservice.checkphonepresence(Phone_number).subscribe((data:any)=>{
        console.log("data without data",data);
        if(data){
          console.log("This phone is data",data);
          this.phonepresentflag = true;
          // this.mess ageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
          if(this.regForm.value.Phone_number!=""){

          this.messageServ.add({severity:'error', summary:'Phone Number Exist', detail:'Please try another Phone number'});
          }
          return false;
        }
      },(error:any)=>{
        if(this.regForm.value.Phone_number==""){
          this.messageServ.add({severity:'error', summary:'Account Not Exist', detail:'Please try another Phone number'});
          }
        this.phonepresentflag = false;

        console.log("Error");
      })
    }
  }

  register() {
    (<HTMLInputElement> document.getElementById("login")).disabled=true
    console.log(this.emailpresentflag)
    if(!this.emailpresentflag) {
      if(!this.phonepresentflag) {
      this.registerloading = true;
      let input = this.regForm.value ? this.regForm.value : '';
      console.log("This is the form value ",this.regForm.value);
      if(input) {
        this.expertservice.register_user(input).subscribe((data:any)=>{
          if(data.message){
            this.messageServ.add({severity:'success', summary:'User Registered', detail:'Thank you for getting registered with us! Please Login'});
            setTimeout(()=>{
              document.getElementById('close_register').click();
              document.getElementById('login_btn').click();
            },3000);

          }
          this.registerloading= false;
          console.log("Successfully regustered ", data);
          (<HTMLInputElement> document.getElementById("login")).disabled=false

        },(error:any)=>{
          console.log("This is the error i n the registeratiuon ", error);
          (<HTMLInputElement> document.getElementById("login")).disabled=false

        })
      }
    }
    else{
      this.messageServ.add({severity:'error', summary:'Phone number Exist', detail:'Please try another Phone number'});
      (<HTMLInputElement> document.getElementById("login")).disabled=false

    }
    }
    else{
      this.messageServ.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
      (<HTMLInputElement> document.getElementById("login")).disabled=false

    }
  }
  navigatetoblog(){
    this.router.navigate(['/blog']);
  }
  getexpertdata() {
    this.expertservice.getexpertuser(this.logged_id).subscribe((data:any)=>{
      console.log('aaaaaaaaaaaa',data)
      var Dat =data.data;
      this.logged_name = Dat.name ? Dat.name :'';
      // this.logged_email = Dat.email ? Dat.email : '';
      // this.profileimage = data.data.profileImage ? data.data.profileImage : '' ;
      // this.expertData =Dat;
      // this.serial_number = Dat.serial_number ? Dat.serial_number :'';
      // this.profile_summary = Dat.profile_summary ? Dat.profile_summary : '' ;
      // this.youtubelink = Dat.youtubelink ? Dat.youtubelink : '';
      // this.whoareyou = Dat.whoareyou ? Dat.whoareyou : '';
      // this.phone_number = Dat.phone_number ? Dat.phone_number : '';
      // this.countries = Dat.expertise ? Dat.expertise : '';
      // this.address = Dat.address ? Dat.address : '';
      // console.log("inside expertdata",this.profileimage);
      // if(this.profileimage) {
      //   this.profileimageflag =true;
      // }
    },(error:any)=>{
      console.log(error);
    })
  }

  logout() {
    this.logged_id =0;
    localStorage.clear();
    this.router.navigate(['/']);
  }
  changeclose(){
    document.getElementById('close_main').click();
    document.getElementById('close_login').click();
    document.getElementById('close_login1').click();
    document.getElementById('close_login2').click();

  }

  changePassword(){
    (<HTMLInputElement> document.getElementById("change")).disabled=true
    console.log(this.emailpresentflag)

    if(this.emailpresentflag) {
      // if(!this.phonepresentflag) {

    this.btn_actionloading=true;
    var input = this.changePasswordForm.value ? this.changePasswordForm.value : {};

    if(input){
console.log(input)
console.log(input.newpassword)
if(input.newpassword==input.confirmnewpassword){

      this.expertservice.changepassword(this.changePasswordForm.value).subscribe((data:any)=>{
        console.log(data)
        if(data) {
          console.log(data)
          document.getElementById('expertLoginModal').style.display='none';
          document.getElementById('expertChangePasswordModal').style.display='block';
          this.btn_actionloading = false;
       
        }
  
      },(error:any)=>{
        this.btn_actionloading=false;
        setTimeout(()=>{
          this.messageServ.add({severity:'warn', summary:'Invalid Credentials', detail:'Your Email Id or password is incorrect'});
          (<HTMLInputElement> document.getElementById("change")).disabled=false

        },2000);
      })
    }
      else{
        alert('password not match')
      }
    }
      else {
        console.log("Please Enter value");
        (<HTMLInputElement> document.getElementById("change")).disabled=false
      }
 
  }
    else{
      this.messageServ.add({severity:'warn', summary:'Invalid Email id', detail:"Your Email id doesn't Exist in any Account"});
      (<HTMLInputElement> document.getElementById("change")).disabled=false
}
  }
  
    change(){
      document.getElementById('expertLoginModal').style.display='none';
    }


  forgotPassword(){
    (<HTMLInputElement> document.getElementById("forgot")).disabled=true
    console.log(this.emailpresentflag)

    if(this.emailpresentflag) {
    this.btn_actionloading=true;
    var input = this.forgotPasswordForm.value ? this.forgotPasswordForm.value : {};

//     if(input){
// console.log(input)
// console.log(input.newpassword)
// if(input.newpassword==input.confirmnewpassword){

      this.expertservice.forgotpassword(this.forgotPasswordForm.value).subscribe((data:any)=>{
        console.log(data)
        if(data) {
          console.log(data)
          document.getElementById('expertLoginModal').style.display='none';
          document.getElementById('expertForgotPasswordModal').style.display='block';
          this.btn_actionloading = false;
       
        }
  
      },(error:any)=>{
        this.btn_actionloading=false;
        setTimeout(()=>{
          this.messageServ.add({severity:'warn', summary:'Invalid Credentials', detail:'Your Email Id is incorrect'});
          (<HTMLInputElement> document.getElementById("forgot")).disabled=false

        },2000);
      })
   
    }
    else{
      this.messageServ.add({severity:'warn', summary:'Invalid Email id', detail:"Your Email id doesn't Exist in any Account"});
      (<HTMLInputElement> document.getElementById("forgot")).disabled=false
}
  }

  openNav() {
    var checkBox =(<HTMLInputElement> document.getElementById("menu-btn"));
    var dropdown = (<HTMLInputElement> document.getElementById("overlay-dropdown"));
    if (checkBox.checked == true){
      dropdown.style.display = "block";
    } else {
      dropdown.style.display = "none";
    }
      // document.getElementById("overlay-dropdown").style.display = "block";
  }
}
