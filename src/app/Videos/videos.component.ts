import { Component, OnInit } from '@angular/core';
import { ExpertService  } from '../providers/expert.service';
import { GlobalService } from '../providers/global.service';
import { Router,ActivatedRoute,Params } from '@angular/router';
import { DomSanitizer,SafeResourceUrl,SafeUrl } from '@angular/platform-browser';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';

@Component({
    selector: 'app-videos',
    templateUrl: './videos.component.html',
    styleUrls: ['./videos.component.css']
  })
export class VideosComponent implements OnInit{
  articleData:any;
  projectionimageurl:String;
  userarticleData:any;
  search:any;
  limit:number=0;
  expertarticleData:any;
  videosummary:any;
  video_tags:any;
  stag = [];
  youtubelink:any;
  contentcategory:any='';
  title:any;
  selectedItems : any[];
  expertData:any;

  constructor(public sanitizer: DomSanitizer,public messageService : MessageService, public expertservice : ExpertService,public router:Router,public globalservice:GlobalService) {
  this.projectionimageurl  = this.globalservice.serverip() + 'articles/projections/'
  }

  ngOnInit() {
    this.getallarticles();
    this.recentArticles();
    // this.getrecentuserarticles();
    this.getexpertrecentarticles();
    // this.getsearchedarticle();
  }
  experts(){
    if(this.search){
    localStorage.setItem('search',this.search)
    }
    this.router.navigate(['/experts'])

  }
  videos(){
    if(this.search){
    localStorage.setItem('search',this.search)
    }
    this.router.navigate(['/videos'])

  }
  getallarticles(){
    console.log(this.limit)
    var body={
        limit:this.limit+8,
        type:'video'

    }
    console.log(body)
    this.expertservice.getallarticles(body).subscribe((data:any)=>{
      console.log("You got the data",data);
      this.limit=this.limit+8

      this.articleData = data;
      if(localStorage.getItem('search')){
        this.searchName(localStorage.getItem('search'))
      }
    },(error:any)=>{
      console.log("You got error while getting articles",error);
    })
 }



 videoarticlesave(){
  alert('105')
  console.log(this.selectedItems)
  alert('106')

  console.log(this.selectedItems.length)
  alert('107')

  console.log(this.videosummary)
  alert('108')

  if(this.selectedItems && this.selectedItems.length){
    for(var u=0;u<this.selectedItems.length;u++){
      var exprt= {
        tag:this.selectedItems[u].tag
      }
      this.stag.push(exprt);
    }
  }
  alert('109')

  var articledata;
  console.log(this.expertData)
  alert('110')

  if(this.expertData.isEnabled){
   articledata =  {
    articletype:'video',
    user_id : localStorage.user_id,
    contentcategory: this.contentcategory,
    title:this.title,
    article_tags:this.stag,
    summary: this.videosummary,
    youtubelink: this.youtubelink,
    draft:false
  }
  console.log(articledata)
  this.expertservice.savearticle(articledata).subscribe((data:any)=>{
    this.messageService.add({severity:'success', summary:'Article Saved', detail:'Thank you for sharing your openion'});
    setTimeout(()=>{
    document.getElementById('close_video_modal').click();
    },3000);
  },(error:any)=>{
    console.log("you got the error",error);
  })
}else{
  articledata =  {
    articletype:'video',
    user_id : localStorage.user_id,
    contentcategory: this.contentcategory,
    title:this.title,
    article_tags:this.stag,
    summary: this.videosummary,
    youtubelink: this.youtubelink,
    draft:true
  }
  console.log(articledata)
  this.expertservice.savearticle(articledata).subscribe((data:any)=>{
    this.messageService.add({severity:'success', summary:'Article Saved', detail:'Thank you for sharing your openion'});
    setTimeout(()=>{
    document.getElementById('close_video_modal').click();
    },3000);
  },(error:any)=>{
    console.log("you got the error",error);
  })
}
}



 // getrecentuserarticles() {
 //   this.expertservice.getrecentuserarticles().subscribe((data:any)=>{
 //     console.log('You got the user data ---  ',data);
 //     this.userarticleData = data.data;
 //   },(error:any)=>{
 //     console.log("you got the error while getting user articles ",error);
 //   })
 // }

 singlearticle(articleId) {
   if(articleId){
  this.router.navigate(['/article',articleId]);
   }
 }
 getexpertrecentarticles() {
   this.expertservice.getexpertrecentarticles().subscribe((data:any)=>{
     console.log('You got the user data ---  ',data);
     this.expertarticleData = data.data;
   },(error:any)=>{
     console.log("you got the error while getting user articles ",error);
   })
 }
 articlecat(category) {
  console.log("you got category",category);
  this.router.navigate(['/articlecat',category]);

  setTimeout(()=>{
  this.globalservice.updatearticles_category('category changed');
},500);
}


recentArticles(){
  this.expertservice.getrecentarticles().subscribe((data:any)=>{
    console.log("You got the data",data);
    this.userarticleData = data;
  },(error:any)=>{
    console.log("You got error while getting articles",error);
  })
}

articletag(tag) {
  console.log("you got category",tag);
  this.router.navigate(['/articlecat',tag]);

  setTimeout(()=>{

  for(var i=0; i<this.articleData.length; i++){
    // console.log(this.allArticleData[i])
    // this.articleData.push(this.allArticleData[i])
     for(var j=0; j<this.articleData[i]['article_tags'].length; j++){
        var datatag=this.articleData[i]['article_tags'][j]['item_text']
        if(tag==datatag){
          console.log('this.allArticleData[i]')
          this.articleData.push(this.articleData[i])
        }
     }
  }
  // this.globalservice.updatearticles_category('category changed');
},500);
}

searchName(event){
  console.log(event)
  // this.articleData=this.articleData;
  // console.log((<HTMLInputElement>document.getElementById("mySearch")).value)
  
  var input, filter, ul, li, a, i;
  if(!localStorage.getItem('search')){
    input = (<HTMLInputElement>document.getElementById("mySearch")).value;
  }
  if(localStorage.getItem('search')){
    input = event;
  }
  this.search=input;
  console.log("iiiiiiiiiiiiiiiiii",input)
  filter = input.toUpperCase();
  console.log(filter)

  var body =
    {
      "name" : filter,
      "searchin" : "videos"
    }
  
      this.expertservice.getsearchedarticle(body).subscribe((data:any)=>{
        console.log("this is the first search",data);
        // this.body = data.data;
        //  for(var t=0;t<data.data.length;t++){
        // this.expertisearray.push(data.data[t]);
        //  }          
            this.articleData = data;

},)


  ul = document.getElementById("grid");
  console.log(ul)
  li = ul.getElementsByClassName("item");
  localStorage.removeItem('search')
  console.log('liiiiiiiiiiii',li)
  console.log(li.length)
  setTimeout(function() {
      for (i = 0; i < li.length; i++) {
      console.log('particular lli', li[i])
      
          a = li[i].getElementsByTagName("h3")[0];
          console.log(li[i].getElementsByTagName("h3")[0])
          if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            console.log('show',li[i])
              li[i].style.display = "";
          } else {
            console.log('hidden',li[i])

              li[i].style.display = "none";
          }
      }

  },100);
 


}
}
