import { Component, OnInit,EventEmitter } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { ExpertService } from '../providers/expert.service';
import { GlobalService } from '../providers/global.service';
import { Router,ActivatedRoute, Params } from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
import {RatingModule} from 'primeng/rating';
import { DomSanitizer  } from '@angular/platform-browser';
@Component({
  selector: 'app-expertpublic',
  templateUrl: './expertpublic.component.html',
  styleUrls: ['./expertpublic.component.css']
})
export class ExpertpublicComponent implements OnInit {
  logged_name:string;
  logged_email:string;
  logged_id :string;
  profilepercentage:any;
  profileimage:string;
  profile_summary:string;
  serial_number:string;
  expertise:any=[];
  imageformflag:Boolean =true;
  youtubelink:string;
  profile_description:any;
  profileimageflag:Boolean=false;
  profileimageurl:string;
  expertData:any;
  expertid:any;
  follower:0;
  followed:0;
  constructor(private sanetizer:DomSanitizer,public expertservice : ExpertService,  private activatedRoute:ActivatedRoute,public globalservice: GlobalService,
      public messageService : MessageService,public router:Router) {
    this.profileimageurl = this.globalservice.serverip() + 'public/experts/';
    this.activatedRoute.params.subscribe((params)=>{
      this.expertid = params['expert_id'];
    })
  }

  ngOnInit() {
    this.logged_name = localStorage.user_name;
    this.logged_email = localStorage.user_email;
    this.logged_id = localStorage.user_id;
    this.getexpertuser();
  }
  getexpertuser() {
    this.expertservice.getexpertuser(this.expertid  ).subscribe((data:any)=>{
      if(data) {
        console.log("You got the data######",data);
        if(data.status==200) {
          var Dat = data.data;
          console.log("DAT expertise",Dat.expertise);
          console.log("percentage",data.data.profile_status_percentage);
          this.profilepercentage=Dat.profile_status_percentage;
          this.profileimage=Dat.profileImage;
          this.serial_number = Dat.serial_number;
          this.expertise = Dat.expertise;
          this.profile_summary = Dat.profile_summary;
          this.youtubelink = Dat.youtubelink;
          this.expertData = data.data;
          this.follower=data.data.follower.length;
          this.followed=data.data.followed.length;
          this.profile_description = this.sanetizer.bypassSecurityTrustHtml(Dat.profile_description);
          // this.countries = Dat.expertise;
          console.log("This is profile image",this.profileimage);
          if(this.profileimage){
            this.profileimageflag =true;
            this.imageformflag  =false;
          }

        }
      }
    },(error:any)=>{
      console.log(error);
    })
  }
}
