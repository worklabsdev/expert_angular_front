import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertpublicComponent } from './expertpublic.component';

describe('ExpertpublicComponent', () => {
  let component: ExpertpublicComponent;
  let fixture: ComponentFixture<ExpertpublicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertpublicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertpublicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
