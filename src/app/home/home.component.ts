import { Component, OnInit } from '@angular/core';
import { ExpertService  } from '../providers/expert.service';
import { GlobalService } from '../providers/global.service';
import { Router,ActivatedRoute,Params } from '@angular/router';
import { FormGroup, FormControl,Validators } from '@angular/forms';
// import * as $ from 'jquery';

declare var $: any;

// interface JQuery {
//   flexslider():void;
// }

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  

})
export class HomeComponent implements OnInit {
  articleData:any;
  projectionimageurl:String;
  userarticleData:any;
  expertData:any;
  expertimagesurl:string;
  postcount:number=0;
  expertcount:number=0;
  searchingdata:any;
  searchingexperts:any;
  searchingarticles:any;
  searchingvideos:any;
  articleForm : FormGroup;
  limit:number=0;
  search:any;
  expertlimit:number=0;
  changemade:Boolean=false;

  countries: any[];

  filteredCountriesSingle: any[];

  filteredCountriesMultiple: any[];
    
  users: any[] = [{ name: 'John' }, { name: 'Jane' }, { name: 'Mario' }];
  userFilter: any = { name: '' };



  // slides:any;
  slides = [
    {img: "./assets/images/blueberry.jpg",
     title: "Ashu",
     certificate: "Certificate name, PHd, Some other degree",
     description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, possimus!"
  },
  {img: "./assets/images/blueberry.jpg",
  title: "Ashu",
  certificate: "Certificate nbbbbbbbbbbbbbame, PHd, Some other degree",
  description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, possimus!"
  },
  {img: "./assets/images/blueberry.jpg",
  title: "Ashu",
     certificate: "Certificate name, PHd, Some other degree",
     description: "Lorem ipsum doggghhhhhhhhhhhhhlor sit amet consectetur adipisicing elit. Quod, possimus!"
  },
  {img: "./assets/images/blueberry.jpg",
  title: "Ashu",
  certificate: "Certificate name, PHd, Some other degree",
  description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, possimus!"
  },
  {img: "./assets/images/blueberry.jpg",
  title: "Ashu",
  certificate: "Certificate name, PHd, Some other degree",
  description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, possimus!"
  }

    
    
  ];
  slideConfig = {"slidesToShow": 4, "slidesToScroll": 1,"autoplay": false, "autoplaySpeed": 2000};
 



  
  



//   public vegetableData: { [key: string]: Object }[] = [
//     { Vegetable: 'Cabbage', Category: 'Leafy and Salad', Id: 'item1' },
//     { Vegetable: 'Chickpea', Category: 'Beans', Id: 'item2' },
//     { Vegetable: 'Garlic', Category: 'Bulb and Stem', Id: 'item3' },
//     { Vegetable: 'Green bean', Category: 'Beans', Id: 'item4' },
//     { Vegetable: 'Horse gram', Category: 'Beans', Id: 'item5' },
//     { Vegetable: 'Nopal', Category: 'Bulb and Stem', Id: 'item6' },
//     { Vegetable: 'Onion', Category: 'Bulb and Stem', Id: 'item7' },
//     { Vegetable: 'Pumpkins', Category: 'Leafy and Salad', Id: 'item8' },
//     { Vegetable: 'Spinach', Category: 'Leafy and Salad', Id: 'item9' },
//     { Vegetable: 'Wheat grass', Category: 'Leafy and Salad', Id: 'item10' },
//     { Vegetable: 'Yarrow', Category: 'Leafy and Salad', Id: 'item11' }
// ];

// public groupFields: Object = { groupBy: 'Category', value: 'Vegetable' };

// public groupWaterMark: string = 'e.g. Cabbage';


  constructor(public expertservice : ExpertService,public router:Router,public globalservice:GlobalService) {
  this.projectionimageurl  = this.globalservice.serverip() + 'articles/projections/';
  this.expertimagesurl = this.globalservice.serverip() + 'public/experts/';
  
  }
   
  






  ngOnInit() {
// document.getElementsByClassName('flexslider carousel')

    

    
    this.articleForm =  new FormGroup({
      contentname: new FormControl('',[Validators.required]),
      contentcategory :new FormControl('',[Validators.required])
      // summary: new FormControl('',[Validators.required]),
      // headerimage: new FormControl('',[Validators.required]),
      // projectionimage : new FormControl(''),
      // reference: new FormControl(''),
      // youtubelink: new FormControl(''),
      // contentcategory:new FormControl(''),
      // article_date :new FormControl('')
    })
    // this.articleForm.value.contentcategory='Article'
    this.getallarticles();
    this.getrecentuserarticles();
    this.getallexperts();
    this.getpostcount();
    this.getexpertcount();
 //this.searching('i');

  }
  getallarticles(){
    var body={
      limit:this.limit+8
  }
    this.expertservice.getallarticles(body).subscribe((data:any)=>{
      console.log("You got the data",data);
      this.articleData = data;
    },(error:any)=>{
      console.log("You got error while getting articles",error);
    })
 }
 savesearch(){
 console.log('lllllllllllllllllll',this.articleForm.value)
// if(this.articleForm.value.contentcategory==""){
//   this.articleForm.value.contentcategory='Article';

// }


 if (this.articleForm.value.contentcategory=='Article'){
     if(this.articleForm.value.contentname){
      this.router.navigate(['/blog'])
      localStorage.setItem('search',this.articleForm.value.contentname)

     }
 }
 else if(this.articleForm.value.contentcategory=='Expert'){
     if(this.articleForm.value.contentname){
            this.router.navigate(['/experts'])
            localStorage.setItem('search',this.articleForm.value.contentname)

     }
 }
 else if(this.articleForm.value.contentcategory=='Video'){
  if(this.articleForm.value.contentname){
        this.router.navigate(['/videos'])
        localStorage.setItem('search',this.articleForm.value.contentname)

  }
}
 }

 getrecentuserarticles() {
   this.expertservice.getrecentuserarticles().subscribe((data:any)=>{
     console.log('You got the user data ---  ',data);
     this.userarticleData = data.data;
   },(error:any)=>{
     console.log("you got the error while getting user articles ",error);
   })
 }
 singlearticle(articleId) {
   if(articleId){
  this.router.navigate(['/article',articleId]);
   }
 }
 getallexperts(){
  var body={
    limit:this.expertlimit+8
  }
   this.expertservice.getallexperts(body).subscribe((data:any)=>{
     console.log("this isthe data",data);
     this.expertData = data.data;
     this.expertlimit=this.expertlimit+8
   },(error:any)=>{
     console.log("error",error);
   })
 }

 getpostcount() {
   this.expertservice.getpostcount().subscribe((count:any)=>{
     console.log("this is the data)))))",count);
     this.postcount = count.data;
   },(error:any)=>{
     console.log("thisi s the error",error);
   })
 }

 getexpertcount(){
   this.expertservice.getexpertcount().subscribe((count:any)=>{
     console.log("this is the  expert  count data)))))",count);
     this.expertcount = count.data;
   },(error:any)=>{
     console.log("thisi s the error",error);
   })
 }

 searchthis(name,cat):void{
  //  alert("1")
// console.log((<HTMLInputElement>document.getElementById("searchings")))
console.log(name)
console.log(cat)

this.articleForm.value.contentname=name;
this.articleForm.value.contentcategory=cat;

(<HTMLInputElement>document.getElementById("searchings")).value=name;
 }


searching(event){
  
  console.log(event)
  // console.log((<HTMLInputElement>document.getElementById("mySearch")).value)
  var input, filter, ul, li, a, i;
  console.log((<HTMLInputElement>document.getElementById("searchings")).value)
  // if(!localStorage.getItem('search')){
  //   input = (<HTMLInputElement>document.getElementById("mySearch")).value;
  // }
  // if(localStorage.getItem('search')){
    // input = event.key;
  // }
  input=(<HTMLInputElement>document.getElementById("searchings")).value;
  console.log("iiiiiiiiiiiiiiiiii",input)
  this.search=input;
  filter = input.toUpperCase();
  console.log('iiiiiiii',filter)
  var body =
  {
    "name" : filter,
    // "searchin" : "expert"
  }
  this.expertservice.searching(body).subscribe((count:any)=>{
    console.log("this is the  expert  count data)))))",count);
    this.searchingdata = count;
    this.searchingarticles=count.articles;
    this.searchingvideos=count.videos;
    this.searchingexperts=count.experts;
    document.getElementById("searcheditems").style.display='block';
  },(error:any)=>{
    console.log("thisi s the error",error);
  })
  // let searcheditems= document.getElementsByClassName('searched-items');
  // searcheditems.style.display='block';
 


}

filterCountryMultiple(event) {
  let query = event.query;
  this.expertservice.getexpertise().subscribe(countries => {
    // console.log("this is countries",countries);
    countries=this.searchingdata;
    this.filteredCountriesMultiple = this.filterCountry(query, this.searchingdata);

  

   
  });
}
filterCountry(query, countries: any[]):any[] {
  console.log(query)
  this.changemade= true;
  //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
  console.log("countries length",length);
  let filtered : any[] = [];
  if(this.searchingdata.articles != undefined){


  for(let i = 0; i < this.searchingdata.articles.length; i++) {

    let country = this.searchingdata.articles[i];
    if(country.title.toLowerCase().indexOf(query.toLowerCase()) != -1) {
      filtered.push(country);
     
    }
    if(i==this.searchingdata.articles.length-1){
      console.log("filtered",filtered);
      return filtered;
    }
  }
  
}
if(this.searchingdata.experts != undefined){

    for(let i = 0; i < this.searchingdata.experts.length; i++) {
      let country = this.searchingdata.experts[i];
      if(country.name.toLowerCase().indexOf(query.toLowerCase()) != -1) {
        country['title']=country.name
        filtered.push(country);
       
      }
      // if(i==this.searchingdata.experts.length-1){
      //   console.log("filtered",filtered);
      //   return filtered;
      // }
    }
  }
  if(this.searchingdata.videos != undefined){

      for(let i = 0; i < this.searchingdata.videos.length; i++) {
        let country = this.searchingdata.videos[i];
        if(country.title.toLowerCase().indexOf(query.toLowerCase()) != -1) {
          filtered.push(country);
         
        }
        if(i==this.searchingdata.videos.length-1){
          console.log("filtered",filtered);
          return filtered;
        }
      }
    }
  }
 
}