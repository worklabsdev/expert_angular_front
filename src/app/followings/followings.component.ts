import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute,Params } from '@angular/router';
import { ExpertService } from '../providers/expert.service';




@Component({
  selector: 'app-Followings',
  templateUrl: './followings.component.html',
  styleUrls: ['./followings.component.css']
})
export class FollowingsComponent implements OnInit {
  followers=[];
  limit:number=0;
  constructor(public expertservice:ExpertService){}
 
  ngOnInit() {
    this.getfollowers()
  }
  getfollowers(){
    var logged_id=localStorage.getItem('user_id');
    this.expertservice.getexpertuser(logged_id).subscribe((data:any)=>{
      console.log('aaaaaaaaaaaa',data)
      var Dat =data.data;
      var followerlist=Dat.followed
      console.log(followerlist)
      if(followerlist){
        var body={
          followerlist:followerlist,
          limit:this.limit+8
      }
      console.log('hhhhhhhhhhh',body)
        this.expertservice.getfollowed(body).subscribe((followerlists:any)=>{
          console.log('kkkkkkkkkkkkkkkkkk',followerlists.length)
          this.followers=followerlists
          this.limit=this.limit+8;
          if(followerlists.length==0 || followerlists.length!=this.limit){
            document.getElementById('seemore').style.display='none';
            document.getElementById('notfound').style.display='block';
                      }
                      else{
                        document.getElementById('seemore').style.display='block';
document.getElementById('notfound').style.display='none';
                      }
        },(error:any)=>{
          console.log(error);
        })
      }
   
    },(error:any)=>{
      console.log(error);
    })
  }
  

}
