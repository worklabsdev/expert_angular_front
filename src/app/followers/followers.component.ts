import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute,Params } from '@angular/router';
import { ExpertService } from '../providers/expert.service';




@Component({
  selector: 'app-Followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.css']
})
export class FollowersComponent implements OnInit {
  followerslist=[];
  limit:number=0;
  constructor(public expertservice:ExpertService){}
 
 
  ngOnInit() {
    this.getfollowing()

  }
  
  getfollowing(){
    var logged_id=localStorage.getItem('user_id');
    this.expertservice.getexpertuser(logged_id).subscribe((data:any)=>{
      console.log('aaaaaaaaaaaa',data)
      var Dat =data.data;
      var followerlist=Dat.follower
      console.log(followerlist)
      if(followerlist){
        var body={
          followerlist:followerlist,
          limit:this.limit+8
      }
      console.log('eeeeeeeeeee',body)
        this.expertservice.getfollowers(body).subscribe((followerslists:any)=>{
          console.log('kkkkkkkkkkkkkkkkkk',followerslists.length)
          this.followerslist=followerslists;
          this.limit=this.limit+8
          if(followerslists.length==0 || followerslists.length!=this.limit){
document.getElementById('seemore').style.display='none';
document.getElementById('notfound').style.display='block';
          }else{
            document.getElementById('seemore').style.display='block';
document.getElementById('notfound').style.display='none';
          }
        },(error:any)=>{
          console.log(error);
        })
      }
   
    },(error:any)=>{
      console.log(error);
    })
  }
  

}
