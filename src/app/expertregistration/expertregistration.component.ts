import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { ExpertInterface } from './expertInterface';
import { ExpertService } from '../providers/expert.service';
import { GlobalService } from '../providers/global.service';
import { RouterModule,Router }  from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';

import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';

@Component({
  selector: 'app-expertregistration',
  templateUrl: './expertregistration.component.html',
  styleUrls: ['./expertregistration.component.css']
})
export class ExpertregistrationComponent implements OnInit {
  expertList :ExpertInterface[]=[];
  loginForm : FormGroup;
  regForm : FormGroup;
  msg : Message[]=[];
  sub:any;
  checkCount: number=0;
  btn_actionloading:Boolean = false;
  emailpresentflag:Boolean =true;
  phonepresentflag:Boolean =true;

  registerloading:Boolean =false;
  constructor(public globalservice: GlobalService,public expertservice:ExpertService,private socialAuthService: AuthService,public router:Router,public messageService : MessageService) { }

  ngOnInit() {
    (<HTMLInputElement> document.getElementById("information_submit")).disabled=true

    this.regForm = new FormGroup({
      name: new FormControl('',Validators.required),
      phone_number: new FormControl('',[Validators.required, Validators.minLength(10),Validators.maxLength(10)]),
      email :new FormControl('',[Validators.required,Validators.email]),
      password : new FormControl('',[Validators.required,Validators.minLength(7)]),
      confirmPassword : new FormControl('',[Validators.required,Validators.minLength(7)]),

    })


  }

  checkemailpresence() {
    let email = this.regForm.value.email;
    console.log(email)
    if(email) {
      this.expertservice.checkemailpresence(email).subscribe((data:any)=>{
        console.log("data without data",data);
        if(data){
          console.log("This mail is data",data);
          this.emailpresentflag = true;
          // this.mess ageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
          this.messageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
          return false;
        }
      },(error:any)=>{
        this.emailpresentflag = false;

        console.log("Error");
      })
    }
  }

  checkphonepresence() {
    let Phone_number = this.regForm.value.phone_number;
    console.log(Phone_number)
    if(Phone_number) {
      this.expertservice.checkphonepresence(Phone_number).subscribe((data:any)=>{
        console.log("data without data",data);
        if(data){
          console.log("This phone is data",data);
          this.phonepresentflag = true;
          // this.mess ageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
          this.messageService.add({severity:'error', summary:'Phone Number Exist', detail:'Please try another Phone_number'});
          return false;
        }
      },(error:any)=>{
        this.phonepresentflag = false;

        console.log("Error");
      })
    }
  }


  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      console.log("PROVIDER_ID",GoogleLoginProvider.PROVIDER_ID);
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log("This is user data" ,userData);
        if(userData.provider=='google'){
          this.expertservice.checkgid(userData.id).subscribe((gdata:any)=>{

            if(gdata) {
              console.log("This is gid",gdata);
              localStorage.setItem('user_name',gdata.name);
              localStorage.setItem('user_email',gdata.email);
              localStorage.setItem('user_id',gdata._id);
              console.log("username"+ localStorage.user_name + "email" + localStorage.email);
              document.getElementById('close_main').click();
              this.router.navigate(['dashboard']);
            }
          },(gerror:any)=>{
            console.log("Thats gerror",gerror);
            if(gerror.status==400) {
              this.expertservice.savegoogleid(userData).subscribe((data:any)=>{
                if(userData.token) {
                  localStorage.setItem('token',userData.token);
                  localStorage.setItem('user_name',userData.name);
                  localStorage.setItem('user_email',userData.email);
                  localStorage.setItem('user_id',data._id);
                  document.getElementById('close_main').click();
                  this.router.navigate(['/dashboard']);
                }
              },(error:any)=>{
                console.log("Got error saving the google id",error);
              })
            }
          })
        }
        else if(userData.provider=='facebook') {
          this.expertservice.checkfbid(userData.id).subscribe((fbdata:any)=>{
            if(fbdata.status == 200) {
              console.log("This is fid",fbdata);
              localStorage.setItem('user_name',fbdata.name);
              localStorage.setItem('user_email',fbdata.email);
              localStorage.setItem('user_id',fbdata._id);
              console.log("username"+ localStorage.user_name + "email" + localStorage.email);
              document.getElementById('close_main').click();
              this.router.navigate(['dashboard']);
            }
          },(fberror:any)=>{
            this.expertservice.savefacebookid(userData).subscribe((data:any)=>{
              if(userData.token) {
                localStorage.setItem('token',userData.token);
                localStorage.setItem('user_name',userData.name);
                localStorage.setItem('user_email',userData.email);
                localStorage.setItem('user_id',data._id);
                document.getElementById('close_main').click();
                this.router.navigate(['/dashboard']);
              }
            },(error:any)=>{
              console.log("Got error saving the google id",error);
            })
          })
        }

      }
    );
  }
   
  checkterms(){ 
    if(this.checkCount==0){
        (<HTMLInputElement> document.getElementById("information_submit")).disabled=false;
        (<HTMLInputElement> document.getElementById("exampleCheck1")).checked=true;
        (<HTMLInputElement> document.getElementById("exampleCheck2")).checked=true;

        this.checkCount=this.checkCount+1
    }
    else{
      (<HTMLInputElement> document.getElementById("information_submit")).disabled=true;
      (<HTMLInputElement> document.getElementById("exampleCheck1")).checked=false;
      (<HTMLInputElement> document.getElementById("exampleCheck2")).checked=false;

      this.checkCount=0;
    }
  }

  register() {
    // (<HTMLInputElement> document.getElementById("information_submit")).disabled=true
    console.log(this.phonepresentflag)
    console.log(this.emailpresentflag)

    if(!this.phonepresentflag) {
    if(!this.emailpresentflag) {
      this.registerloading = true;
      let input = this.regForm.value ? this.regForm.value : '';
      console.log("This is the form value ",this.regForm.value);
      if(input) {
        console.log(input)
        if(input.password==input.confirmPassword){
         this.expertservice.register_user(input).subscribe((data:any)=>{
          if(data.message){
            this.messageService.add({severity:'success', summary:'Thank You for registering with YellowSquash !', detail:'Please check your email for verification.'});
            setTimeout(()=>{
              document.getElementById('close_register').click();
              document.getElementById('login_btn').click();
            },3000);

          }
          this.registerloading= false;
          console.log("Successfully regustered ", data);
          (<HTMLInputElement> document.getElementById("information_submit")).disabled=false

        },(error:any)=>{
          console.log("This is the error i n the registeratiuon ", error);
          (<HTMLInputElement> document.getElementById("information_submit")).disabled=false

        })
      }
      else{
        this.messageService.add({severity:'error', summary:'Password Mis-Match Error', detail:'Confirm Password not match'});

      }
    }
    else{
      this.messageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
      (<HTMLInputElement> document.getElementById("information_submit")).disabled=false

    }
  }
  else{
    this.messageService.add({severity:'error', summary:'Phone Exist', detail:'Please try another Phone Number'});
    (<HTMLInputElement> document.getElementById("information_submit")).disabled=false

  }
}
  }

  articlecat(category) {
    console.log("you got category",category);
    this.router.navigate(['/articlecat',category]);
  
    setTimeout(()=>{
    this.globalservice.updatearticles_category('category changed');
  },500);
  }
  
}
