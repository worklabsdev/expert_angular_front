import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertregistrationComponent } from './expertregistration.component';

describe('ExpertregistrationComponent', () => {
  let component: ExpertregistrationComponent;
  let fixture: ComponentFixture<ExpertregistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertregistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertregistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
