export interface ExpertInterface {
  name : string,
  email: string,
  password : string,
  phone_number : number,
  confirmPassword : string
}
