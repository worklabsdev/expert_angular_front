import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicExpertSidebarComponent } from './public-expert-sidebar.component';

describe('PublicExpertSidebarComponent', () => {
  let component: PublicExpertSidebarComponent;
  let fixture: ComponentFixture<PublicExpertSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicExpertSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicExpertSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
