import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { ExpertService } from '../providers/expert.service';
// import { Router}  from '@angular/router';
@Component({
  selector: 'app-public-expert-sidebar',
  templateUrl: './public-expert-sidebar.component.html',
  styleUrls: ['./public-expert-sidebar.component.css']
})
export class PublicExpertSidebarComponent implements OnInit {
  expertid:any;
  constructor(private activatedRoute:ActivatedRoute,public expertservice:ExpertService,public router:Router) {
    this.activatedRoute.params.subscribe((params)=>{
      this.expertid = params['expert_id'];
    })
  }

  ngOnInit() {
  }

  followexpert(){
    var user_logged = localStorage.user_id;
    if(user_logged) {
      var postData = {
        expert : this.expertid,
        follower:localStorage.user_id,
        type:'expert'
      }
      this.expertservice.followexpert(postData).subscribe((data:any)=>{
        console.log("this is the data",data);
      },(error:any)=>{
        console.log("this is the error",error);
      })
    }
    else {
      localStorage.setItem('followedexpert',this.expertid);
      document.getElementById('loginmodalbtn').click();
    }
  }
  navigateto_expert_description(){
    this.router.navigate(['/expert',this.expertid]);
  }


}
