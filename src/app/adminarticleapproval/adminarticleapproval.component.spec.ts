import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminarticleapprovalComponent } from './adminarticleapproval.component';

describe('AdminarticleapprovalComponent', () => {
  let component: AdminarticleapprovalComponent;
  let fixture: ComponentFixture<AdminarticleapprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminarticleapprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminarticleapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
