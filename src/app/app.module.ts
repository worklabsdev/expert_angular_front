import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { ExpertService } from './providers/expert.service';
import { GlobalService } from './providers/global.service';
import { HttpModule } from '@angular/http';
import {ChipsModule} from 'primeng/chips';

import {ToastModule} from 'primeng/toast';
import {GrowlModule} from 'primeng/growl';
import {EditorModule} from 'primeng/editor';
import {MessageService} from 'primeng/api';
import {RatingModule} from 'primeng/rating';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AutoCompleteModule} from 'primeng/autocomplete';
// import { MyDatePickerModule } from 'mydatepicker';
// import {AutoCompleteModule} from '@syncfusion/ej2-ng-dropdowns';


import { FilterPipeModule } from 'ngx-filter-pipe';
import { SlickModule } from 'ngx-slick';
import { NgxMasonryModule } from 'ngx-masonry';



import {NgxPaginationModule} from 'ngx-pagination';
import { HttpClientModule } from '@angular/common/http';
import { NgProgressModule } from '@ngx-progressbar/core';
// import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { NgxEditorModule } from 'ngx-editor';
import { SafePipe} from './providers/safehtml.pipe';
import {
    SocialLoginModule,
    AuthServiceConfig,
    GoogleLoginProvider,
    FacebookLoginProvider,
} from "angular-6-social-login";



import { NgCircleProgressModule } from 'ng-circle-progress';
import { AppComponent } from './app.component';
import { AppRouting } from './app.router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './navigation/public/header/header.component';
import { FooterComponent } from './navigation/public/footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ExpertLoginHeaderComponent } from './expert-login-header/expert-login-header.component';
import { ExpertSidebarComponent } from './expert-sidebar/expert-sidebar.component';
import { CreateComponent } from './articles/create/create.component';
import { ListComponent } from './articles/list/list.component';
import { SingleComponent } from './articles/single/single.component';
import { SinglePrivateArticleComponent } from './articles/single-private-article/single-private-article.component';
import { EditArticleComponent } from './articles/edit-article/edit-article.component';
import { ProfileComponent } from './profile/profile.component';
import { ExpertarticlesComponent } from './articles/public/expertarticles/expertarticles.component';
import { BlogComponent } from './blog/blog.component';
import { AdminarticleapprovalComponent } from './adminarticleapproval/adminarticleapproval.component';
import { ExpertregistrationComponent } from './expertregistration/expertregistration.component';
import { ExpertpublicComponent } from './expertpublic/expertpublic.component';
import { ExpertarticlecatComponent } from './articles/expertarticlecat/expertarticlecat.component';
import { ExpertsearchComponent } from './expertsearch/expertsearch.component';
import { ExpertArticlesComponent } from './expert-articles/expert-articles.component';
import { ExpertinfobarComponent } from './expertinfobar/expertinfobar.component';
import { ExpertsComponent } from './experts/experts.component';
import { PublicExpertSidebarComponent } from './public-expert-sidebar/public-expert-sidebar.component';
import { PrivateexpertComponent } from './privateexpert/privateexpert.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AdminExpertComponent } from './admin/expert-profile/expert.component';
import { LoginConfirmComponent } from './login-confirm/login-confirm.component';
import { NewsLetterComponent } from './admin/newsletter/newsletter.component';
import { VideosComponent } from './Videos/videos.component';
import { SingleVideoComponent } from './singlevideo/singlevideo.component';
import { AboutUsComponent } from './aboutUs/aboutUs.component';
import { VisionComponent } from './vision/vision.component';
import { FollowersComponent } from './followers/followers.component';
import { FollowingsComponent } from './followings/followings.component';
import { ArticlesDraftComponent } from './articles/draft/draft.component';



//  Defining the social Id's

// Configs
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        // {
        //   id: 'facebook',
        //   provider: new FacebookLoginProvider("276903913121204")
        // },
        {
          id: 'google',
          // 52272261360-bkfjgcunn43tuujboac8lr5f8efemvrt.apps.googleusercontent.com
          provider: new GoogleLoginProvider("133869438860-7tj0ghja4pk5vcdirgrpfn3ififps2ha.apps.googleusercontent.com")
// 698969137066-vi3c3k4vr636ep53mtt0114cagl7hpli.apps.googleusercontent.com
        }
      ]
  );
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginConfirmComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    ExpertLoginHeaderComponent,
    ExpertSidebarComponent,
    CreateComponent,
    ListComponent,
    SingleComponent,
    SinglePrivateArticleComponent,
    EditArticleComponent,
    ProfileComponent,
    ExpertarticlesComponent,
    SafePipe,
    BlogComponent,
    AdminarticleapprovalComponent,
    ExpertregistrationComponent,
    ExpertpublicComponent,
    ExpertarticlecatComponent,
    ExpertsearchComponent,
    ExpertArticlesComponent,
    ExpertinfobarComponent,
    ExpertsComponent,
    PublicExpertSidebarComponent,
    PrivateexpertComponent,
    AdminExpertComponent,
    NewsLetterComponent,
    VideosComponent,
    SingleVideoComponent,
    FollowingsComponent,
    AboutUsComponent,
    VisionComponent,
    FollowersComponent,
    ArticlesDraftComponent
  ],
  imports: [
    BrowserModule,
    FilterPipeModule,
    AppRouting,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ToastModule,
    BrowserAnimationsModule,
    GrowlModule,
    RatingModule,
    SocialLoginModule,
    EditorModule,
    NgxPaginationModule,
    NgxMasonryModule,
    HttpClientModule,
   NgProgressModule.forRoot(),
   SlickModule.forRoot(),
   NgxEditorModule,
   NgCircleProgressModule.forRoot({
     radius: 100,
     outerStrokeWidth: 16,
     innerStrokeWidth: 8,
     outerStrokeColor: "#78C000",
     innerStrokeColor: "#C7E596",
     animationDuration: 300,
   }),
   ChipsModule,
   AutoCompleteModule,
  
   NgMultiSelectDropDownModule.forRoot()

  //  MyDatePickerModule


  //  NgProgressHttpModule.forRoot()
  ],
  providers: [
    ExpertService,
    GlobalService,
    MessageService,
    {
     provide: AuthServiceConfig,
     useFactory: getAuthServiceConfigs
   }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
