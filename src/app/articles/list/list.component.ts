import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { GlobalService } from '../../providers/global.service';
// import { Articles } from './article';
import { ExpertService } from '../../providers/expert.service';
import { RouterModule,Router }  from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { DomSanitizer,SafeResourceUrl,SafeUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  articleData :any;
  projectionimageurl:any;
  title:any;
  expertData:any;
  videosummary:any;
  video_tags:any;
  msg:Message[]=[];
  youtubelink:any;
  contentcategory:any='';
  trustedDashboardUrl : SafeUrl;
  p: number = 1;
  approvedflag:any=0
  page:number =1 ;
  user_id:any;
  totalPages:number =0;
  feedbackshowflag:number = 0;
  articlecount:boolean=true;
  stag = [];
  filteredCountriesSingle: any[];
  selectedItems : any[];
  filteredCountriesMultiple: any[];
  changemade:Boolean=false;
  tagarray:any[];

  constructor(
    public expertservice:ExpertService,
    private  router:Router,
    public messageService : MessageService,
    public sanitizer: DomSanitizer,
    private globalservice: GlobalService) {
      this.projectionimageurl  = this.globalservice.serverip() + 'articles/projections/';
        this.user_id = localStorage.user_id;

    }


    ngOnInit() {
this.getexpertdata();
      if(this.user_id){
        this.getexpertarticles(this.user_id);
      }
      else {
        console.log("user not logged in");
      }
      console.log("you have to traverse",localStorage.followedexpert);
    }

    getexpertarticles(user_id){
      this.expertservice.getexpertarticles(user_id).subscribe((data:any)=>{
        this.articleData = data;
        console.log(data)
        if(data.length==0){
        this.articlecount=false
        }
        console.log("this is the length",data.length);
        // if(data.length % 6 ==0 || data.length % 6 < 0) {
        //   this.totalPages =1
        // }else {
        this.totalPages = Math.ceil(data.length/6);
        // }


        console.log("this is the total count ",this.totalPages);
      },(error:any)=>{
        console.log("You got error while getting articles",error);
      })
    }
    // changePage(page) {
    //   this.expertservice.getexpertarticles(this.user_id).subscribe((data:any)=>{
    //     this.articleData = data;
    //   },(error:any)=>{
    //     console.log("You got error while getting articles",error);
    //   })
    // }
    navigatesinglearticle(id) {
      this.router.navigate(['/singlearticle',id]);
    }
    getexpertdata() {
      var logged_id=localStorage.getItem('user_id')
      this.expertservice.getexpertuser(logged_id).subscribe((data:any)=>{
        console.log('aaaaaaaaaaaa',data)
        this.expertData=data.data
        // this.getdraftarticle();

      },(error:any)=>{
        console.log(error);
      })
    }
    videoarticlesave(){
      alert('105')
      console.log(this.selectedItems)
      alert('106')

      console.log(this.selectedItems.length)
      alert('107')

      console.log(this.videosummary)
      alert('108')

      if(this.selectedItems && this.selectedItems.length){
        for(var u=0;u<this.selectedItems.length;u++){
          var exprt= {
            tag:this.selectedItems[u].tag
          }
          this.stag.push(exprt);
        }
      }
      alert('109')

      var articledata;
      console.log(this.expertData)
      alert('110')

      if(this.expertData.isEnabled){
       articledata =  {
        articletype:'video',
        user_id : localStorage.user_id,
        contentcategory: this.contentcategory,
        title:this.title,
        article_tags:this.stag,
        summary: this.videosummary,
        youtubelink: this.youtubelink,
        draft:false
      }
      console.log(articledata)
      this.expertservice.savearticle(articledata).subscribe((data:any)=>{
        this.messageService.add({severity:'success', summary:'Article Saved', detail:'Thank you for sharing your openion'});
        setTimeout(()=>{
        document.getElementById('close_video_modal').click();
        },3000);
      },(error:any)=>{
        console.log("you got the error",error);
      })
    }else{
      articledata =  {
        articletype:'video',
        user_id : localStorage.user_id,
        contentcategory: this.contentcategory,
        title:this.title,
        article_tags:this.stag,
        summary: this.videosummary,
        youtubelink: this.youtubelink,
        draft:true
      }
      console.log(articledata)
      this.expertservice.savearticle(articledata).subscribe((data:any)=>{
        this.messageService.add({severity:'success', summary:'Article Saved', detail:'Thank you for sharing your openion'});
        setTimeout(()=>{
        document.getElementById('close_video_modal').click();
        },3000);
      },(error:any)=>{
        console.log("you got the error",error);
      })
    }
    }

    approvearticle(articleid){
      this.expertservice.approvearticle(articleid).subscribe((data:any)=>{
        this.messageService.add({severity:'Approved', summary:'Approved article'});
      },(error:any)=>{
        console.log("thisi s error",error);
      })
    }
    get_en_ds_articles(enabled){
      console.log("thgis enabled statys",enabled);
      this.expertservice.get_en_ds_articles(enabled).subscribe((data:any)=>{
        console.log("this is the data after filter of the status",data);
          this.articleData =data.data;
      },(error:any)=>{
        console.log("You are getting the error ",error);
      })
    }

    getcategoryarticle($event) {
      var category = $event.target.value;
      this.expertservice.getcategoryarticle(category).subscribe((data:any)=>{
        console.log("thisi si te data",data);
        this.articleData = data.data;
      },(error:any)=>{
        console.log("this is the error",error);
      })
    }
    articlecat(category) {
      console.log("you got category",category);
      this.router.navigate(['/articlecat',category]);

      setTimeout(()=>{
      this.globalservice.updatearticles_category('category changed');
    },500);


    }


    gettag(event){
      console.log(event.query);
      this.expertservice.gettag().subscribe((data:any)=>{
        console.log("this is the first log",data.data.length);
        this.tagarray = data.data;
        //  for(var t=0;t<data.data.length;t++){
        // this.tagarrayarray.push(data.data[t]);
        //  }
        console.log("this is the data for tag",this.tagarray);
      },(error:any)=>{
        console.log(error);
      })
    }
    filterCountryMultiple(event) {
      let query = event.query;
      this.expertservice.gettag().subscribe(selectedItems => {
        // console.log("this is selectedItems",selectedItems);
        this.filteredCountriesMultiple = this.filterCountry(query, selectedItems.data);
        console.log(this.filteredCountriesMultiple)
      });
    }
    filterCountry(query, selectedItems: any[]):any[] {
      this.changemade= true;
      //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
      console.log("selectedItems length",length);
      let filtered : any[] = [];
      for(let i = 0; i < selectedItems.length; i++) {
        let country = selectedItems[i];
        console.log("tag ",country.tag);
        if(country.tag.toLowerCase().indexOf(query.toLowerCase()) == 0) {
          filtered.push(country);
        }
      }
      console.log("filtered",filtered);
      return filtered;
    }

  }
