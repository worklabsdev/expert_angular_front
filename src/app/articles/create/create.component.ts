import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { Articles } from './article';
import { ExpertService } from '../../providers/expert.service';
import { RouterModule,Router }  from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  articles : Articles[]=[];
  articleForm : FormGroup;
  msg:Message[]=[];
  article_header_64textString:String;
  article_projection_64textString:String;
  saveloadingflag :any ='';
  // whoareyou:any;
  dropdownList = [];
  selectedItems : any[];
  countries : any[];

  dropdownSettings = {};
  tags=[];
  changemade:Boolean=false;
  tagarray:any[];
  categories:any;
  checkCount: number=0;
  expertData:any;
   stag = [];

  filteredCountriesSingle: any[];

  filteredCountriesMultiple: any[];

    constructor(public expertservice:ExpertService,private  router:Router, public messageService : MessageService) { }
    ngOnInit() {
      this.getexpertdata();
      this.category();
      this.dropdownList = [
        { item_id: 1, item_text: 'Accupressure' },
        { item_id: 2, item_text: 'Acupuncture' },
        { item_id: 3, item_text: 'Allergies (gluten, dairy, etc)' },
        { item_id: 4, item_text: 'Amino Acids' },
        { item_id: 5, item_text: 'Anaemia ' },
        { item_id: 6, item_text: 'Anti - Pollution' },
        { item_id: 7, item_text: 'Anti - inflamatory' },
        { item_id: 8, item_text: 'Anti-cancer' },
        { item_id: 9, item_text: 'Anti-oxidant' },
        { item_id: 10, item_text: 'Anti-oxidant Rich' },
        { item_id: 11, item_text: 'Anti-Pollution' },
        { item_id: 12, item_text: 'Aromatherapy' },
        { item_id: 13, item_text: 'Arthiritis' },
        { item_id: 14, item_text: 'Asthma' },
        { item_id: 15, item_text: 'Ayurveda' },
        { item_id: 16, item_text: 'Balanced Meals' },
        { item_id: 17, item_text: 'Behavioral Therapy' },
        { item_id: 18, item_text: 'Better Digestion' },
        { item_id: 19, item_text: 'Body Building' },
        { item_id: 20, item_text: 'Bone Health' },
        { item_id: 21, item_text: 'Brain Health' },
        { item_id: 22, item_text: 'Breast Cancer' },
        { item_id: 23, item_text: 'Bronchitis' },
        { item_id: 24, item_text: 'Calcium Rich' },
        { item_id: 25, item_text: 'Cancer' },
        { item_id: 26, item_text: 'Cancer Diet  ' },
        { item_id: 27, item_text: 'Cancer Management' },
        { item_id: 28, item_text: 'Carbohydrates' },
        { item_id: 29, item_text: 'Cardiovascular' },
        { item_id: 30, item_text: 'Career Coach' },
        { item_id: 31, item_text: 'Cholesterol ' },
        { item_id: 32, item_text: 'Color therapy' },
        { item_id: 33, item_text: 'COPD' },
        { item_id: 34, item_text: 'Dairy Free' },
        { item_id: 35, item_text: 'De-addiction' },
        { item_id: 36, item_text: 'Detox' },
        { item_id: 37, item_text: 'Diabetes' },
        { item_id: 38, item_text: 'Diabetes Friendly' },
        { item_id: 39, item_text: 'Diabetes Management' },
        { item_id: 40, item_text: 'Elderly Nutrition' },
        { item_id: 41, item_text: 'Executive Coach' },
        { item_id: 42, item_text: 'Eye Health' },
        { item_id: 43, item_text: 'Fasting' },
        { item_id: 44, item_text: 'Fats' },
        { item_id: 45, item_text: 'Fitness' },
        { item_id: 46, item_text: 'Functional Medicine' },
        { item_id: 47, item_text: 'Gastrointestinal (GI)' },
        { item_id: 48, item_text: 'Genetics' },
        { item_id: 49, item_text: 'Gluten Free' },
        { item_id: 50, item_text: 'Good Hair' },
        { item_id: 51, item_text: 'Good Skin' },
        { item_id: 52, item_text: 'Gout/ Uric acid' },
        { item_id: 53, item_text: 'Gut Health' },
        { item_id: 54, item_text: 'Health Food' },
        { item_id: 55, item_text: 'Healthy Desserts' },
        { item_id: 56, item_text: 'Healthy Drink' },
        { item_id: 57, item_text: 'Healthy Eating' },
        { item_id: 58, item_text: 'Healthy Fats' },
        { item_id: 59, item_text: 'Healthy Snacks' },
        { item_id: 60, item_text: 'Heart Health' },
        { item_id: 61, item_text: 'Hepatic Health' },
        { item_id: 62, item_text: 'Herbal' },
        { item_id: 63, item_text: 'High Fiber' },
        { item_id: 64, item_text: 'Homeopathy' },
        { item_id: 65, item_text: 'Hormonal Imbalance' },
        { item_id: 66, item_text: 'Hypertension' },
        { item_id: 67, item_text: 'Hypertension friendly' },
        { item_id: 68, item_text: 'Hypertension management' },
        { item_id: 69, item_text: 'Hypnotherapy' },
        { item_id: 70, item_text: 'Immunity Booster' },
        { item_id: 71, item_text: 'Integrative Nutrition' },
        { item_id: 72, item_text: 'Intermittent Fasting' },
        { item_id: 73, item_text: 'Iron ' },
        { item_id: 74, item_text: 'Iron Rich' },
        { item_id: 75, item_text: 'Joint Pain' },
        { item_id: 76, item_text: 'Keto Diet' },
        { item_id: 77, item_text: 'Kidney' },
        { item_id: 42, item_text: 'Kids Nutrition' },
        { item_id: 42, item_text: 'Lactating mothers' },
        { item_id: 42, item_text: 'Lactose Intolerance' },
        { item_id: 42, item_text: 'Leadership Training' },
        { item_id: 42, item_text: 'Life Skills' },
        { item_id: 42, item_text: 'Lifestyle Management' },
        { item_id: 42, item_text: 'Low Calories' },
        { item_id: 42, item_text: 'Low Fat' },
        { item_id: 42, item_text: 'Low Sodium' },
        { item_id: 42, item_text: 'Lung Health' },
        { item_id: 42, item_text: 'Meditation' },
        { item_id: 42, item_text: 'Microgreens' },
        { item_id: 42, item_text: 'Minerals ' },
        { item_id: 42, item_text: 'Motivational Speaker' },
        { item_id: 42, item_text: 'Naturopathy' },
        { item_id: 42, item_text: 'NLP' },
        { item_id: 42, item_text: 'Nutrigenomics' },
        { item_id: 42, item_text: 'Nutrition for Seniors' },
        { item_id: 42, item_text: 'Omega-3 rich' },
        { item_id: 42, item_text: 'Omega-6 rich' },
        { item_id: 42, item_text: 'Organic' },
        { item_id: 42, item_text: 'Organic Farming' },
        { item_id: 42, item_text: 'Organic Food' },
        { item_id: 42, item_text: 'Osteoporosis' },
        { item_id: 42, item_text: 'Overall Healthy' },
        { item_id: 42, item_text: 'Oxidative stress' },
        { item_id: 42, item_text: 'Paleo Diet' },
        { item_id: 42, item_text: 'PCOS' },
        { item_id: 42, item_text: 'Pilates' },
        { item_id: 42, item_text: 'PMS' },
        { item_id: 42, item_text: 'Prebiotic' },
        { item_id: 42, item_text: 'Pregnancy' },
        { item_id: 42, item_text: 'Pregnancy Nutrition' },
        { item_id: 42, item_text: 'Preteen nutrition' },
        { item_id: 42, item_text: 'Probiotic' },
        { item_id: 42, item_text: 'Productivity Training' },
        { item_id: 42, item_text: 'Protein Rich' },
        { item_id: 42, item_text: 'Proteins' },
        { item_id: 42, item_text: 'Psychology' },
        { item_id: 42, item_text: 'Psychotherapy' },
        { item_id: 42, item_text: 'Public Health' },
        { item_id: 42, item_text: 'Raw Foods' },
        { item_id: 42, item_text: 'Raw Juices' },
        { item_id: 42, item_text: 'Reiki' },
        { item_id: 42, item_text: 'Salads' },
        { item_id: 42, item_text: 'Saturated fat ' },
        { item_id: 42, item_text: 'Skin health' },
        { item_id: 42, item_text: 'Smoking' },
        { item_id: 42, item_text: 'Social Cause' },
        { item_id: 42, item_text: 'Spiritual Healing' },
        { item_id: 42, item_text: 'Sports Nutrition' },
        { item_id: 42, item_text: 'Stress' },
        { item_id: 42, item_text: 'Stress Management' },
        { item_id: 42, item_text: 'Sujok' },
        { item_id: 42, item_text: 'Teenage Nutrition' },
        { item_id: 42, item_text: 'Testing Labs' },
        { item_id: 42, item_text: 'Thyroid' },
        { item_id: 42, item_text: 'Tuberculosis' },
        { item_id: 42, item_text: 'Vegan' },
        { item_id: 42, item_text: 'Vegetarian' },
        { item_id: 42, item_text: 'Vitamins ' },
        { item_id: 42, item_text: 'Water Therapy' },
        { item_id: 42, item_text: 'Weight Loss' },
        { item_id: 42, item_text: 'Yoga' },
     


       
      ];
     
      // this.selectedItems = [
      //   { item_id: 3, item_text: 'Pune' },
      //   { item_id: 4, item_text: 'Navsari' }
      // ];
           
      this.dropdownSettings = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 42,
        allowSearchFilter: true
      };

      this.articleForm =  new FormGroup({
        title: new FormControl('',[Validators.required]),
        article :new FormControl('',[Validators.required]),
        summary: new FormControl('',[Validators.required]),
        headerimage: new FormControl('',[Validators.required]),
        projectionimage : new FormControl(''),
        reference: new FormControl(''),
        youtubelink: new FormControl(''),
        contentcategory:new FormControl(''),
        article_date :new FormControl(''),
        article_tags :new FormControl('')

      })
      // this.articleForm.patchValue({
      //   article_date:new Date()
      // })
    }

    getexpertdata() {
      var logged_id=localStorage.getItem('user_id')
      this.expertservice.getexpertuser(logged_id).subscribe((data:any)=>{
        console.log('aaaaaaaaaaaa',data)
        this.expertData=data.data
        // this.getdraftarticle();

      },(error:any)=>{
        console.log(error);
      })
    }

    category(){
        this.expertservice.getexpertise().subscribe((data:any)=>{
          console.log("this is the first log",data.data.length);
          this.categories = data.data;
        
          console.log("this is the data for expertise",this.categories);
        },(error:any)=>{
          console.log(error);
        })
     
    }
      
    onItemSelect(item: any) {
      console.log(item);
      this.tags.push(item.item_text);
      console.log(this.tags)
    }
    onItemDeSelect(item: any) {
      console.log(item);
      var filtered = this.tags.filter(function(value, index, arr){

        return value != item.item_text;
 
    });
    console.log(filtered)
this.tags=filtered;
      console.log(this.tags)
    }
    onSelectAll(items: any) {
      console.log(items);
    }

    // saveAsDraft(){
    
    checkterms(){ 
      if(this.checkCount==0){
          (<HTMLInputElement> document.getElementById("information_submit")).disabled=false;
          (<HTMLInputElement> document.getElementById("exampleCheck1")).checked=true;
          (<HTMLInputElement> document.getElementById("exampleCheck2")).checked=true;
  
          this.checkCount=this.checkCount+1
      }
      else{
        (<HTMLInputElement> document.getElementById("information_submit")).disabled=true;
        (<HTMLInputElement> document.getElementById("exampleCheck1")).checked=false;
        (<HTMLInputElement> document.getElementById("exampleCheck2")).checked=false;
  
        this.checkCount=0;
      }
    }

      
    // }
   
    

      savearticle() {
        this.saveloadingflag = 1;

         console.log(this.selectedItems)
if(this.selectedItems && this.selectedItems.length){
  for(var u=0;u<this.selectedItems.length;u++){
    var exprt= {
      tag:this.selectedItems[u].tag
    }
    this.stag.push(exprt);
  }
}
//   setTimeout(function(){ alert("Hello");
//   console.log(this.selectedItems)

//   console.log(this.stag)
//  }, 3000);
//  console.log(this.stag)

        

        console.log("this is dte",this.articleForm.value.contentcategory);
        var postdata;
        if(this.expertData.isEnabled==true){
console.log(this.expertData.isEnabled)

         postdata = {
        title: this.articleForm.value.title,
        article : this.articleForm.value.article,
        user_id : localStorage.user_id,
        summary: this.articleForm.value.summary,
        headerimage : this.article_header_64textString,
        projectionimage :this.article_projection_64textString,
        reference: this.articleForm.value.reference,
        youtubelink:this.articleForm.value.youtubelink,
        contentcategory :this.articleForm.value.contentcategory,
        articletype:'article',
        article_date:this.articleForm.value.article_date,
        article_tags:this.stag,
        // expertise:rat,

      };
 
    }
    else{
      console.log(this.expertData.isEnabled)
      
       postdata = {
         
        title: this.articleForm.value.title,
        article : this.articleForm.value.article,
        user_id : localStorage.user_id,
        summary: this.articleForm.value.summary,
        headerimage : this.article_header_64textString,
        projectionimage :this.article_projection_64textString,
        reference: this.articleForm.value.reference,
        youtubelink:this.articleForm.value.youtubelink,
        contentcategory :this.articleForm.value.contentcategory,
        articletype:'article',
        article_date:this.articleForm.value.article_date,
        article_tags:this.stag,
        // expertise:rat,
        draft:true
      };
  
    }

      console.log(postdata)
      this.expertservice.savearticle(postdata).subscribe((data:any)=>{
        if(data){
          console.log(data)
          this.messageService.add({severity:'success', summary:'Article Saved', detail:'Thank you for sharing your openion'});
          this.saveloadingflag ='';
          // this.getdraftarticle();
          setTimeout(()=>{
            this.router.navigate(['/articles']);
          },1500);
        }
          console.log("Success",data);
      },(error:any)=>{
        this.messageService.add({severity:'warn', summary:'Article Not Saved', detail:'Something went wrong'});
          this.saveloadingflag ='';
        console.log("You got error",error);
      })
        console.log("Result of form ", this.articleForm.value.post);
   
      }

      back() {
        // this.globalservice.back();
          window.history.back();
      }

      handleFileSelect(evt){
        var files = evt.target.files;
        var file = files[0];

        if (files && file) {
          var reader = new FileReader();

          reader.onload =this._handleReaderLoaded.bind(this);

          reader.readAsBinaryString(file);
        }
      }

      _handleReaderLoaded(readerEvt) {
        var binaryString = readerEvt.target.result;
        this.article_header_64textString= btoa(binaryString);
        this.article_header_64textString = 'data:image/png;base64,' +this.article_header_64textString;
        // console.log(btoa(binaryString));
      }

      handleprojectionFileSelect(evt){
        var files = evt.target.files;
        var file = files[0];
         if(files && file) {
           var reader = new FileReader();
           reader.onload = this._projectionhandleReaderLoaded.bind(this);
           reader.readAsBinaryString(file);
         }
      }

      _projectionhandleReaderLoaded(readerEvt) {
        var binaryString = readerEvt.target.result;
        this.article_projection_64textString = btoa(binaryString);
        this.article_projection_64textString  = 'data:image/png;base64,' + this.article_projection_64textString;
      }


      // getdraftarticle(){
      //   // console.log("you are getting Id in front",this.articleId);
      //   console.log(this.expertData._id)
      //   var body={
      //     expertid:this.expertData._id
      //   }
      //   console.log(body)
      //   this.expertservice.getdraftarticle(body).subscribe((data:any)=>{
      //     console.log("This is the data ",data);
      //     this.tags=data.article_tags
      //     var tag
      //     for(var i=0;i<data.article_tags.length;i++){
      //       tag={
      //         item_text:data.article_tags[i],
      //         item_id:i+1
  
      //       }
      //       this.onItemSelect(tag)
  
      //     }
      //     (<HTMLInputElement> document.getElementById("submitbutton")).style.display='none';

      //     (<HTMLInputElement> document.getElementById("draftbutton")).style.display='block';
      //   //  data.title
      //   this.articleForm.setValue({
        
      //     title:"abc",
      //     article:data.article,
      //     summary:data.summary,
      //     headerimage:data.headerimage,
      //     projectionimage:data.projectionimage,
      //     reference:data.reference,
      //     youtubelink:data.youtubelink,
      //     contentcategory:data.contentcategory,
          
      //     article_date:data.article_date,
      //   })
      //   // this.article_projection_64textString=data.projectionimage
      //   // this.article_header_64textString=data.headerimage

      
      //           },(error:any)=>{
      //     console.log("Error while fetching the articles ",error);
      //     (<HTMLInputElement> document.getElementById("draftbutton")).style.display='none';

      //     (<HTMLInputElement> document.getElementById("submitbutton")).style.display='block';

      //   })
      // }

      gettag(event){
        console.log(event.query);
        this.expertservice.gettag().subscribe((data:any)=>{
          console.log("this is the first log",data.data.length);
          this.tagarray = data.data;
          //  for(var t=0;t<data.data.length;t++){
          // this.tagarrayarray.push(data.data[t]);
          //  }
          console.log("this is the data for tag",this.tagarray);
        },(error:any)=>{
          console.log(error);
        })
      }
      filterCountryMultiple(event) {
        let query = event.query;
        this.expertservice.gettag().subscribe(selectedItems => {
          // console.log("this is selectedItems",selectedItems);
          this.filteredCountriesMultiple = this.filterCountry(query, selectedItems.data);
          console.log(this.filteredCountriesMultiple)
        });
      }
      filterCountry(query, selectedItems: any[]):any[] {
        this.changemade= true;
        //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
        console.log("selectedItems length",length);
        let filtered : any[] = [];
        for(let i = 0; i < selectedItems.length; i++) {
          let country = selectedItems[i];
          console.log("tag ",country.tag);
          if(country.tag.toLowerCase().indexOf(query.toLowerCase()) == 0) {
            filtered.push(country);
          }
        }
        console.log("filtered",filtered);
        return filtered;
      }
   
}
