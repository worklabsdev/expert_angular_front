export interface Articles {
  title:string,
  article:string,
  user_id:string,
  summary:string,
  headerimage:string,
  projectionimage:string,
  reference:string,
  youtubelink:string,
  article_date:string
}
