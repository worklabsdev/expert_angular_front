import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { ExpertService } from '../../providers/expert.service';
import { GlobalService } from '../../providers/global.service';
import { Router,ActivatedRoute, Params } from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.css']
})
export class EditArticleComponent implements OnInit {
  articleData:any;
  feedback:string
  expertData:any;
  msg:Message[]=[];
  editarticleForm:FormGroup;
  headerimageurl:any;
  projectionimageurl:any;
  article_header_64textString:String;
  article_projection_64textString:String;
  createdAt:Date;
  dropdownList = [];
  dropdownSettings = {};
  tags=[];
  changemade:any = false;
  stag = [];
  selectedItems : any[];


  tagarray:any[];
  filteredCountriesSingle: any[];

  filteredCountriesMultiple: any[];

  constructor(private activatedRoute:ActivatedRoute,public expertservice:ExpertService,public globalservice: GlobalService,
    public messageService : MessageService,public router:Router) {
      this.headerimageurl  = this.globalservice.serverip() + 'articles/headers/'
      this.projectionimageurl = this.globalservice.serverip() + 'articles/projections/'
    }

    ngOnInit() {
      this.getsinglearticle();
      this.dropdownList = [
        { item_id: 1, item_text: 'amino acids' },
        { item_id: 2, item_text: 'anti-aging' },
        { item_id: 3, item_text: 'anti-inflammatory' },
        { item_id: 4, item_text: 'anti-oxidant rich' },
        { item_id: 5, item_text: 'carbohydrates' },
        { item_id: 6, item_text: 'brain health' },
        { item_id: 7, item_text: 'cholesterol' },
        { item_id: 8, item_text: 'breast cancer' },
        { item_id: 9, item_text: 'cancer management' },
        { item_id: 10, item_text: 'cholesterol-friendly foods' },
        { item_id: 11, item_text: 'diabetes' },
        { item_id: 12, item_text: 'diabetes-friendly foods' },
        { item_id: 13, item_text: 'diabetes management' },
        { item_id: 14, item_text: 'diet' },
        { item_id: 15, item_text: 'fats' },
        { item_id: 16, item_text: 'fitness' },
        { item_id: 17, item_text: 'gestational diabetes' },
        { item_id: 18, item_text: 'good hair' },
        { item_id: 19, item_text: 'good skin' },
        { item_id: 20, item_text: 'gut health' },
        { item_id: 21, item_text: 'healthy drink' },
        { item_id: 22, item_text: 'healthy eating' },
        { item_id: 23, item_text: 'healthy fats' },
        { item_id: 23, item_text: 'heart healthy' },
        { item_id: 24, item_text: 'hypertension' },
        { item_id: 25, item_text: 'hypertension management' },
        { item_id: 26, item_text: 'immune system' },
        { item_id: 27, item_text: 'immunity' },
        { item_id: 28, item_text: 'ingredients' },
        { item_id: 29, item_text: 'low calorie' },
        { item_id: 30, item_text: 'minerals' },
        { item_id: 31, item_text: 'nutrition' },
        { item_id: 32, item_text: 'Pregnancy' },
        { item_id: 33, item_text: 'pregnancy nutrition' },
        { item_id: 34, item_text: 'proteins' },
        { item_id: 35, item_text: 'saturated fat' },
        { item_id: 36, item_text: 'stress' },
        { item_id: 37, item_text: 'trace elements' },
        { item_id: 38, item_text: 'trans fat' },
        { item_id: 39, item_text: 'type 1 diabetes' },
        { item_id: 40, item_text: 'type 2 diabetes' },
        { item_id: 41, item_text: 'vitamins' },
        { item_id: 42, item_text: 'weight loss' },
       
      ];
      this.dropdownSettings = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 42,
        allowSearchFilter: true
      };
    }

    getsinglearticle() {
      const allParams = this.activatedRoute.snapshot.params // allParams is an object
      const articleid = allParams.articleId
      console.log("you are getting Id in front",articleid);
      this.expertservice.getsinglearticle(articleid).subscribe((data:any)=>{
        console.log("This is the data ",data);
        this.articleData = data.articledata;
        this.expertData = data.userdata;
        // this.articleData.article_date = new Date(this.articleData.article_date);
        console.log('this is articledata',this.articleData);
        },(error:any)=>{
        console.log("Error while fetching the articles ",error);
      })
    }

    singlearticle(id){
      this.router.navigate(['/singlearticle',id])
    }

    handleFileSelect(evt){
      var files = evt.target.files;
      var file = files[0];

      if (files && file) {
        var reader = new FileReader();

        reader.onload =this._handleReaderLoaded.bind(this);

        reader.readAsBinaryString(file);
      }
    }

    _handleReaderLoaded(readerEvt) {
      var binaryString = readerEvt.target.result;
      this.article_header_64textString= btoa(binaryString);
      this.article_header_64textString = 'data:image/png;base64,' +this.article_header_64textString;
      // console.log(btoa(binaryString));
    }

    handleprojectionFileSelect(evt){
      var files = evt.target.files;
      var file = files[0];
      if(files && file) {
        var reader = new FileReader();
        reader.onload = this._projectionhandleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
      }
    }

    _projectionhandleReaderLoaded(readerEvt) {
      var binaryString = readerEvt.target.result;
      this.article_projection_64textString = btoa(binaryString);
      this.article_projection_64textString  = 'data:image/png;base64,' + this.article_projection_64textString;
    }

          
    onItemSelect(item: any) {
      console.log(item);
      this.tags.push(item.item_text);
      console.log(this.tags)
    }
    onItemDeSelect(item: any) {
      console.log(item);
      var filtered = this.tags.filter(function(value, index, arr){

        return value != item.item_text;
 
    });
    console.log(filtered)
this.tags=filtered;
      console.log(this.tags)
    }
    onSelectAll(items: any) {
      console.log(items);
    }

    editarticle(){
      const allParams = this.activatedRoute.snapshot.params // allParams is an object
      const articleid = allParams.articleId;
      // var header_image='';

      console.log(this.selectedItems)
      if(this.selectedItems && this.selectedItems.length){
        for(var u=0;u<this.selectedItems.length;u++){
          var exprt= {
            tag:this.selectedItems[u].tag
          }
          this.stag.push(exprt);
        }
      }


      if(this.article_header_64textString) {
        var header_image= this.article_header_64textString;
      }
      else {
        header_image = this.articleData.headerimage;
      }

      if(this.article_projection_64textString) {
        var projection_image = this.article_projection_64textString;
      }
      else {
        projection_image =  this.articleData.projectionimage;
      }
      var updateData
      console.log(this.articleData)
if(this.expertData.isEnabled){
   updateData = {
    contentcategory: this.articleData.contentcategory,
    title:this.articleData.title,
    summary : this.articleData.summary,
    article : this.articleData.article,
    reference :  this.articleData.reference,
    youtubelink :  this.articleData.youtubelink,
    headerimage :  header_image,
    projectionimage :projection_image,
    user_id : localStorage.user_id,
    draft:false,
    articletype:this.articleData.articletype,
    article_tags:this.stag,
    mailsentbyadmin:true

  };
  console.log("Updated Data",updateData);
  this.expertservice.editarticle(articleid,updateData).subscribe((data:any)=>{
    console.log("returned data",data);
    if(data.status==200) {
      this.messageService.add({severity:'success', summary:'Article Updated'});
      // window.location.reload();
      setTimeout(()=>{
        window.scrollTo(0,0);
        // this.router.navigate(['/articles']);
      },1000);
    }
    console.log(data);
  },(error:any)=>{
    console.log(error);
  })
  // this.expertservice.savearticle(updateData).subscribe((data:any)=>{
  //   console.log("returned data",data);
  //   if(data) {
  //     console.log(data)
  //     this.messageService.add({severity:'success', summary:'Article Saved', detail:'Thank you for sharing your openion'});
  //     // this.getdraftarticle();
  //     setTimeout(()=>{
  //       this.router.navigate(['/articles']);
  //     },1000);
  //   }
  //   console.log(data);
  // },(error:any)=>{
  //   console.log(error);
  // })
}else{
     updateData = {
      contentcategory: this.articleData.contentcategory,
      title:this.articleData.title,
      summary : this.articleData.summary,
      article : this.articleData.article,
      reference :  this.articleData.reference,
      youtubelink :  this.articleData.youtubelink,
      headerimage :  header_image,
      projectionimage :projection_image,
      user_id : localStorage.user_id,
      draft:true,
      articletype:this.articleData.articletype,
      article_tags:this.stag,
      feedback: this.feedback,
      };
      console.log("Updated Data",updateData);
      this.expertservice.editarticle(articleid,updateData).subscribe((data:any)=>{
        console.log("returned data",data);
        if(data.status==200) {
          this.messageService.add({severity:'success', summary:'Article Updated'});
          // window.location.reload();
          setTimeout(()=>{
            window.scrollTo(0,0);
            // this.router.navigate(['/articles']);
          },1000);
        }
        console.log(data);
      },(error:any)=>{
        console.log(error);
      })
    }
    }

    back(){
      window.history.back();
    }

     sendfeedback(){
      const allParams = this.activatedRoute.snapshot.params // allParams is an object
      const articleid = allParams.articleId;
      // var header_image='';
      if(this.article_header_64textString) {
        var header_image= this.article_header_64textString;
      }
      else {
        header_image = this.articleData.headerimage;
      }

      if(this.article_projection_64textString) {
        var projection_image = this.article_projection_64textString;
      }
      else {
        projection_image =  this.articleData.projectionimage;
      }


      var updateData = {
        contentcategory: this.articleData.contentcategory,
        title:this.articleData.title,
        summary : this.articleData.summary,
        article : this.articleData.article,
        reference :  this.articleData.reference,
        youtubelink :  this.articleData.youtubelink,
        headerimage :  header_image,
        projectionimage :projection_image,
        feedback: this.feedback

      };
      console.log("Updated Data",updateData);
      this.expertservice.editarticleforfeedback(articleid,updateData).subscribe((data:any)=>{
        console.log("returned data",data);
        if(data.status==200) {
          this.messageService.add({severity:'success', summary:'Article Updated'});
          // window.location.reload();
          setTimeout(()=>{
            window.scrollTo(0,0);
            // this.router.navigate(['/articles']);
          },1000);
        }
        console.log(data);
      },(error:any)=>{
        console.log(error);
      })
     }

     approvearticle() {
      const allParams = this.activatedRoute.snapshot.params // allParams is an object
      const articleid = allParams.articleId;
      
      this.expertservice.approvearticle(articleid).subscribe((data:any)=>{
        if(data.status==200) {
          this.messageService.add({severity:'success', summary:'Article approved and posted'});
        }
        console.log()
      },(error:any)=>{
        console.log(error);
      })
    }

    gettag(event){
      console.log(event.query);
      this.expertservice.gettag().subscribe((data:any)=>{
        console.log("this is the first log",data.data.length);
        this.tagarray = data.data;
        //  for(var t=0;t<data.data.length;t++){
        // this.tagarrayarray.push(data.data[t]);
        //  }
        console.log("this is the data for tag",this.tagarray);
      },(error:any)=>{
        console.log(error);
      })
    }
    filterCountryMultiple(event) {
      let query = event.query;
      this.expertservice.gettag().subscribe(selectedItems => {
        // console.log("this is selectedItems",selectedItems);
        this.filteredCountriesMultiple = this.filterCountry(query, selectedItems.data);
        console.log(this.filteredCountriesMultiple)
      });
    }
    filterCountry(query, selectedItems: any[]):any[] {
      this.changemade= true;
      //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
      console.log("selectedItems length",length);
      let filtered : any[] = [];
      for(let i = 0; i < selectedItems.length; i++) {
        let country = selectedItems[i];
        console.log("tag ",country.tag);
        if(country.tag.toLowerCase().indexOf(query.toLowerCase()) == 0) {
          filtered.push(country);
        }
      }
      console.log("filtered",filtered);
      return filtered;
    }

  }
