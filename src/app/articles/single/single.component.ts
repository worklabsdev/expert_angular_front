import { Component, OnInit } from '@angular/core';
import { ExpertService } from '../../providers/expert.service';
import { GlobalService } from '../../providers/global.service';
import { Router,ActivatedRoute, Params } from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {MessageService} from 'primeng/components/common/messageservice';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.css']
})
export class SingleComponent implements OnInit {
  articleData:any;
  expertData:any;
  headerimageurl:any;
  expertimageurl:any;
  maincontent:any;
  articleId:any;
  newsletter:any;
  loggeduserid:any;
  articleForm : FormGroup;
  tags:any;

  constructor(public messageService : MessageService, private activatedRoute:ActivatedRoute,public expertservice:ExpertService,public globalservice: GlobalService,private sanitizer:DomSanitizer) {
    this.headerimageurl  = this.globalservice.serverip() + 'articles/headers/'
    this.expertimageurl = this.globalservice.serverip() + 'public/experts/';
    this.articleId = this.activatedRoute.snapshot.params.articleId;
  }

  ngOnInit() {
    if(localStorage.user_id) {
      this.loggeduserid = localStorage.user_id;
    }
    this.articleForm =  new FormGroup({

      summary: new FormControl('',[Validators.required]),
    
    })
    const allParams = this.activatedRoute.snapshot.params // allParams is an object
    const articleid = allParams.articleId
    this.getsinglearticle();
  }

  getsinglearticle() {
    console.log("you are getting Id in front",this.articleId);
    this.expertservice.getsinglearticle(this.articleId).subscribe((data:any)=>{
      console.log("This is the data ",data);
      this.articleData = data.articledata;
      this.tags=this.articleData.article_tags;
      this.maincontent = this.articleData.article;
      this.expertData = data.userdata;
      this.maincontent =  this.sanitizer.bypassSecurityTrustHtml(this.maincontent);
      console.log("This is expert data",this.expertData);
      // this.articleBasedOnTags();

    },(error:any)=>{
      console.log("Error while fetching the articles ",error);
      // this.articleBasedOnTags();

    })
  }
  sendnewsletter() {
    var newsletteremail = this.newsletter ?  this.newsletter :'';
    if(newsletteremail) {
      this.expertservice.sendnewsletter(this.articleId,newsletteremail).subscribe((data:any)=>{
        console.log(data);
      },(error:any)=>{
        console.log(error);
      })
    }
    else {
      console.log("Please enter the newsletteremail");
    }

  }

  savearticle(id){
    // alert("ok")
    console.log(id)
    console.log(this.articleForm.value.summary)
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();

    // var newdate = day + "/" + month + "/" + year;
    var commentData=[];
    commentData.push({
      article_id: id, 
      article_comment: this.articleForm.value.summary,
      comment_date: day + "/" + month + "/" + year,
      user_id: localStorage.getItem('user_id'),
      user_name: localStorage.getItem('user_name'),
      user_email: localStorage.getItem('user_email')

  });
      this.expertservice.commentClick(commentData[0]).subscribe((data:any)=>{
        // this.like = true;
        // this.likeCount=this.likeCount+1;
        console.log("successfully add comment",data)
        this.messageService.add({severity:'success', summary:'comment add successfully'});
        this.getsinglearticle();
        this.articleForm.reset();
        },(error:any)=>{
        console.log("this is error",error);
      })
  }

  articleBasedOnTags(){
    console.log(this.tags)

    var body =
  {
    "name" : this.tags[0].item_text,
    "searchin" : "tags"
  }
  console.log(body)
  this.expertservice.getsearchedarticle(body).subscribe((count:any)=>{
    console.log("this is the  expert  count data)))))",count);
    // this.searchingdata = count;
    // this.searchingarticles=count.articles;
    // this.searchingvideos=count.videos;
    // this.searchingexperts=count.experts;
    // document.getElementById("searcheditems").style.display='block';
  },(error:any)=>{
    console.log("thisi s the error",error);
  })
  }

}
