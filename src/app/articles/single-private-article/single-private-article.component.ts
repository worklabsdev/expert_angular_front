import { Component, OnInit ,SecurityContext} from '@angular/core';
import { ExpertService } from '../../providers/expert.service';
import { GlobalService } from '../../providers/global.service';
import { Router,ActivatedRoute, Params } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { FormGroup, FormControl,Validators } from '@angular/forms';

@Component({
  selector: 'app-single-private-article',
  templateUrl: './single-private-article.component.html',
  styleUrls: ['./single-private-article.component.css']
})
export class SinglePrivateArticleComponent implements OnInit {

  articleData:any;
  maincontent:any;
  headerimageurl:any;
  referencesData:any[]=[];
  approvedflag:any =0;
  like=false;
  likeCount=0;
  articleForm : FormGroup;
  tags:any;
  // slides:any[];
  // slideConfig:any;
  // whoareyou:any;
slides:any;
  // slides = [
  //   {img: "./assets/images/blueberry.jpg",
  //    related: "Ashu",
  //    read: "70 read",
  //    title: "avacoda is .....",
  //    content:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, possimus!",
  //    link:"#"
  // },
  // {img: "./assets/images/blueberry.jpg",
  // related: "Ashu",
  // read: "70 read",
  // title: "avacoda is .....",
  // content:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, possimus!",
  // link:"#"
  // },
  // {img: "./assets/images/blueberry.jpg",
  // related: "Ashu",
  // read: "70 read",
  // title: "avacoda is .....",
  // content:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, possimus!",
  // link:"#"
  // },
  // {img: "./assets/images/blueberry.jpg",
  // related: "Ashu",
  // read: "70 read",
  // title: "avacoda is .....",
  // content:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, possimus!",
  // link:"#"
  // },
  // {img: "./assets/images/blueberry.jpg",
  // related: "Ashu",
  // read: "70 read",
  // title: "avacoda is .....",
  // content:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, possimus!",
  // link:"#"
  // }

    
    
  // ];
  slideConfig = {"slidesToShow": 3, "slidesToScroll": 1,"autoplay": true, "autoplaySpeed": 2000};
 


  constructor(private activatedRoute:ActivatedRoute,public expertservice:ExpertService,
    public globalservice: GlobalService,public router:Router,private sanitizer:DomSanitizer,public messageService : MessageService) {
      this.headerimageurl  = this.globalservice.serverip() + 'articles/headers/'
    }

    ngOnInit() {


      
   

      this.articleForm =  new FormGroup({

        summary: new FormControl('',[Validators.required]),
      
      })
      const allParams = this.activatedRoute.snapshot.params // allParams is an object
      const articleid = allParams.articleId
      this.getsinglearticle(articleid);
    }

    getsinglearticle(articleid) {
      console.log("you are getting Id in front",articleid);
      this.expertservice.getsingleprivatearticle(articleid).subscribe((data:any)=>{
        if(data.isEnabled) {
          this.approvedflag=1;
          this.tags=data.article_tags;
              this.articleBasedOnTags();
              
        }
        console.log("This is the data ",data);

        this.articleData = data;
        var comm =Array.of(this.articleData.reference);

        for(var t=0;t<this.articleData.reference;t++) {
          this.referencesData.push(comm);
        }

        for(var t=0;t<this.articleData.favourite.length;t++) {
            this.likeCount=this.likeCount+1;
            if(this.articleData['favourite'][t]['user_id']==localStorage.getItem('user_id')){
              this.like=true;
            }
        }
        // this.referencesData.push(Array.of(this.articleData.reference));
        console.log("reference",this.referencesData);
        // this.articleData.article= this.sanitizer.bypassSecurityTrustResourceUrl(this.articleData.article);
        this.maincontent = this.sanitizer.bypassSecurityTrustHtml(this.articleData.article);
      },(error:any)=>{
        console.log("Error while fetching the articles ",error);
      })
    }
    redirecteditarticle(id){
      this.router.navigate(['/editsinglearticle',id]);
    }

    approvearticle() {
      const allParams = this.activatedRoute.snapshot.params
      const articleid = allParams.articleId
      this.expertservice.approvearticle(articleid).subscribe((data:any)=>{
        console.log("Approved",data);
        this.approvedflag = 1;
        this.messageService.add({severity:'success', summary:'Updated successfully'});
      },(error:any)=>{
        console.log("thisi s error",error);
      })
    }



    likeClick(id){
    var likeData=[];
    likeData.push({
      article_id: id, 
      user_id:  localStorage.getItem('user_id')
  });

      this.expertservice.LikeClick(likeData[0]).subscribe((data:any)=>{
        this.like = true;
        this.likeCount=this.likeCount+1;
        this.messageService.add({severity:'success', summary:'like successfully'});
      },(error:any)=>{
        console.log("thisi s error",error);
      })
    }
    unlikeClick(id){
      var unlikeData=[];
      unlikeData.push({
        article_id: id, 
        user_id:  localStorage.getItem('user_id')
    });

        this.expertservice.UnlikeClick(unlikeData[0]).subscribe((data:any)=>{
          this.like = false;
          this.likeCount=this.likeCount-1;
          this.messageService.add({severity:'success', summary:'unlike successfully'});
        },(error:any)=>{
          console.log("thisi s error",error);
        })
      }
      savearticle(id){
        // alert("ok")
        console.log(id)
        console.log(this.articleForm.value.summary)
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        // var newdate = day + "/" + month + "/" + year;
        var commentData=[];
        commentData.push({
          article_id: id, 
          article_comment: this.articleForm.value.summary,
          comment_date: day + "/" + month + "/" + year,
          user_id: localStorage.getItem('user_id'),
          user_name: localStorage.getItem('user_name'),
          user_email: localStorage.getItem('user_email')

      });
          this.expertservice.commentClick(commentData[0]).subscribe((data:any)=>{
            // this.like = true;
            // this.likeCount=this.likeCount+1;
            console.log("successfully add comment",data)
            this.messageService.add({severity:'success', summary:'comment add successfully'});
            this.getsinglearticle(id);
            this.articleForm.reset();
            },(error:any)=>{
            console.log("this is error",error);
          })
      }
      articleBasedOnTags(){
        console.log(this.tags)
    
        var body =
      {
        "name" : this.tags[0].item_text,
        "searchin" : "tags"
      }
      console.log(body)
      this.expertservice.getsearchedarticle(body).subscribe((count:any)=>{
        console.log("this is the  expert  count data)))))",count);
        this.slides=count;
        // this.searchingdata = count;
        // this.searchingarticles=count.articles;
        // this.searchingvideos=count.videos;
        // this.searchingexperts=count.experts;
        // document.getElementById("searcheditems").style.display='block';
      },(error:any)=>{
        console.log("thisi s the error",error);
      })
      }
  }
