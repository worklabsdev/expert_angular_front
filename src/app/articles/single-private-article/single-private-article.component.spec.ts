import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglePrivateArticleComponent } from './single-private-article.component';

describe('SinglePrivateArticleComponent', () => {
  let component: SinglePrivateArticleComponent;
  let fixture: ComponentFixture<SinglePrivateArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinglePrivateArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglePrivateArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
