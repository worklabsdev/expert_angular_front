import { Component, OnInit } from '@angular/core';
import { ExpertService  } from '../../providers/expert.service';
import { GlobalService } from '../../providers/global.service';
import { Router,ActivatedRoute,Params } from '@angular/router';
@Component({
  selector: 'app-expertarticlecat',
  templateUrl: './expertarticlecat.component.html',
  styleUrls: ['./expertarticlecat.component.css']
})
export class ExpertarticlecatComponent implements OnInit {
  articleData:any;
  projectionimageurl:String;
  userarticleData:any;
  articlecategory:any;
  articlecatdata:any;
  catparam:any;
  limit:number=0;
  allArticleData:any;
  constructor(public expertservice : ExpertService,public router:Router,public globalservice:GlobalService,public activatedRoute: ActivatedRoute) {
    this.projectionimageurl  = this.globalservice.serverip() + 'articles/projections/';
    this.articlecategory = this.activatedRoute.snapshot.params.cat;

    this.activatedRoute.params.subscribe(params => {
      console.log("these are the params",params.cat);
      this.catparam = params.cat;
    })
    this.articlecatdata = this.globalservice.getArticleCat().subscribe(message=>{
      this.getcategoryarticle(this.catparam);
    })

  }

  ngOnInit() {
    this.recentArticles()
    this.getallarticles()
    this.getcategoryarticle(this.catparam);
  }
  getallarticles(){
    var body={
      limit:this.limit+8
  }
    this.expertservice.getallarticles(body).subscribe((data:any)=>{
      console.log("You got the data",data);
      if(data){
        this.allArticleData = data;
      }
    },(error:any)=>{
      console.log("You got error while getting articles",error);
    })
  }
  getcategoryarticle(cat){
    this.expertservice.getcategoryarticle(cat).subscribe((data:any)=>{
      console.log("You got the data",data);
      if(data.data){
        this.articleData = data.data;
      }
    },(error:any)=>{
      console.log("You got error while getting articles",error);
    })

  }
  singlearticle(articleId) {
    if(articleId){
      this.router.navigate(['/article',articleId]);
    }
  }
  articlecat(category) {
    console.log("you got category",category);
    this.router.navigate(['/articlecat',category]);

    setTimeout(()=>{
    this.globalservice.updatearticles_category('category changed');
  },500);
}
articletag(tag) {
  console.log("you got category",tag);
  this.router.navigate(['/articlecat',tag]);

  setTimeout(()=>{

  for(var i=0; i<this.allArticleData.length; i++){
    // console.log(this.allArticleData[i])
    // this.articleData.push(this.allArticleData[i])
     for(var j=0; j<this.allArticleData[i]['article_tags'].length; j++){
        var datatag=this.allArticleData[i]['article_tags'][j]['item_text']
        if(tag==datatag){
          console.log('this.allArticleData[i]')
          this.articleData.push(this.allArticleData[i])
        }
     }
  }
  // this.globalservice.updatearticles_category('category changed');
},500);
}

recentArticles(){
    this.expertservice.getrecentarticles().subscribe((data:any)=>{
      console.log("You got the data",data);
      this.userarticleData = data;
    },(error:any)=>{
      console.log("You got error while getting articles",error);
    })
}

searchName(event){
  console.log(event)
  this.articleData=this.allArticleData;
  // console.log((<HTMLInputElement>document.getElementById("mySearch")).value)
  var input, filter, ul, li, a, i;
  if(!localStorage.getItem('search')){
  input = (<HTMLInputElement>document.getElementById("mySearch")).value;
  }
  if(localStorage.getItem('search')){
    input = event;
    }
  
  filter = input.toUpperCase();
  console.log(filter)
  ul = document.getElementById("grid");
  console.log(ul)
  li = ul.getElementsByClassName("item");
  localStorage.removeItem('search')
  console.log('liiiiiiiiiiii',li)
  console.log(li.length)
  setTimeout(function() {
  for (i = 0; i < li.length; i++) {
  console.log('particular lli', li[i])
  
      a = li[i].getElementsByTagName("h3")[0];
      console.log(li[i].getElementsByTagName("h3")[0])
      if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
        console.log('show',li[i])
          li[i].style.display = "";
      } else {
        console.log('hidden',li[i])

          li[i].style.display = "none";
      }
  }

},100);
}

}
