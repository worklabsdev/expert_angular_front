import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertarticlecatComponent } from './expertarticlecat.component';

describe('ExpertarticlecatComponent', () => {
  let component: ExpertarticlecatComponent;
  let fixture: ComponentFixture<ExpertarticlecatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertarticlecatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertarticlecatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
