import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertarticlesComponent } from './expertarticles.component';

describe('ExpertarticlesComponent', () => {
  let component: ExpertarticlesComponent;
  let fixture: ComponentFixture<ExpertarticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertarticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertarticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
