import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { GlobalService } from '../../providers/global.service';
// import { Articles } from './article';
import { ExpertService } from '../../providers/expert.service';
import { RouterModule,Router }  from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { DomSanitizer,SafeResourceUrl,SafeUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-draft',
  templateUrl: './draft.component.html',
  styleUrls: ['./draft.component.css']
})
export class ArticlesDraftComponent implements OnInit {
  articleData :any;
  projectionimageurl:any;
  title:any;
    msg:Message[]=[];
  youtubelink:any;
  contentcategory:any='';
  trustedDashboardUrl : SafeUrl;
  p: number = 1;
  approvedflag:any=0
  page:number =1 ;
  user_id:any;
  totalPages:number =0;
  feedbackshowflag:number = 0;
  articlecount:boolean=true;
  constructor(
    public expertservice:ExpertService,
    private  router:Router,
    public messageService : MessageService,
    public sanitizer: DomSanitizer,
    private globalservice: GlobalService) {
      this.projectionimageurl  = this.globalservice.serverip() + 'articles/projections/';
        this.user_id = localStorage.user_id;

    }


    ngOnInit() {

      if(this.user_id){
        this.getexpertarticles(this.user_id);
      }
      else {
        console.log("user not logged in");
      }
      console.log("you have to traverse",localStorage.followedexpert);
    }

    getexpertarticles(user_id){
      var body={
        expertid:user_id,
      }
      this.expertservice.getdraftarticle(body).subscribe((data:any)=>{
        this.articleData = data;
        console.log(data)
        if(data.length==0){
        this.articlecount=false
        }
        console.log("this is the length",data.length);
        // if(data.length % 6 ==0 || data.length % 6 < 0) {
        //   this.totalPages =1
        // }else {
        this.totalPages = Math.ceil(data.length/6);
        // }


        console.log("this is the total count ",this.totalPages);
      },(error:any)=>{
        console.log("You got error while getting articles",error);
      })
    }
    // changePage(page) {
    //   this.expertservice.getexpertarticles(this.user_id).subscribe((data:any)=>{
    //     this.articleData = data;
    //   },(error:any)=>{
    //     console.log("You got error while getting articles",error);
    //   })
    // }
    navigatesinglearticle(id) {
      this.router.navigate(['/editsinglearticle',id]);
    }
    videoarticlesave(){
      var articledata =  {
        articletype:'video',
        user_id : localStorage.user_id,
        contentcategory: this.contentcategory,
        title:this.title,
        youtubelink: this.youtubelink
      }
      this.expertservice.savearticle(articledata).subscribe((data:any)=>{
        this.messageService.add({severity:'success', summary:'Article Saved', detail:'Thank you for sharing your openion'});
        setTimeout(()=>{
        document.getElementById('close_video_modal').click();
        },3000);
      },(error:any)=>{
        console.log("you got the error",error);
      })
    }

    approvearticle(articleid){
      this.expertservice.approvearticle(articleid).subscribe((data:any)=>{
        this.messageService.add({severity:'Approved', summary:'Approved article'});
      },(error:any)=>{
        console.log("thisi s error",error);
      })
    }
    get_en_ds_articles(enabled){
      console.log("thgis enabled statys",enabled);
      this.expertservice.get_en_ds_articles(enabled).subscribe((data:any)=>{
        console.log("this is the data after filter of the status",data);
          this.articleData =data.data;
      },(error:any)=>{
        console.log("You are getting the error ",error);
      })
    }

    getcategoryarticle($event) {
      var category = $event.target.value;
      this.expertservice.getcategoryarticle(category).subscribe((data:any)=>{
        console.log("thisi si te data",data);
        this.articleData = data.data;
      },(error:any)=>{
        console.log("this is the error",error);
      })
    }
    articlecat(category) {
      console.log("you got category",category);
      this.router.navigate(['/articlecat',category]);

      setTimeout(()=>{
      this.globalservice.updatearticles_category('category changed');
    },500);


    }


}
