
import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { loginConfirmInterface } from './login-confirmInterface';
import { ExpertService } from '../providers/expert.service';
import { GlobalService } from '../providers/global.service';
import { RouterModule,Router, ActivatedRoute }  from '@angular/router';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';

import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';

@Component({
  selector: 'app-login-confirm',
  templateUrl: './login-confirm.component.html',
  styleUrls: ['./login-confirm.component.css']
})
export class LoginConfirmComponent implements OnInit {
  expertList :loginConfirmInterface[]=[];
  loginForm : FormGroup;
  regForm : FormGroup;
  msg : Message[]=[];
  sub:any;
  btn_actionloading:Boolean = false;
  emailpresentflag:Boolean =false;
  registerloading:Boolean =false;
  constructor(
    // private api: ApiService,
  //  private toast: ToastrService,
  //  private cookies: CookieService,
   private route: ActivatedRoute,
   public globalservice: GlobalService,public expertservice:ExpertService,private socialAuthService: AuthService,public router:Router,public messageService : MessageService) { }

  ngOnInit() {
    this.regForm = new FormGroup({
      name: new FormControl('',Validators.required),
      email :new FormControl('',[Validators.required,Validators.email]),
      password : new FormControl('',[Validators.required,Validators.minLength(7)]),
    })
    this.route.params.subscribe(params => {
      var confirmCode = params["confirmCode"];
      this.confirmAccount(confirmCode);
    });
    setTimeout(()=>{
      document.getElementById('close_register').click();
      document.getElementById('login_btn').click();
    },3000);
  }

  public confirmAccount(confirmCode) {
    this.expertservice.getaccountconfirm(confirmCode).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          this.messageService.add({severity:'success', summary:'Account Confirmed', detail:'Now You can Log In'});

          // this.toast.success("Now You can Log In");
          // this.toast.success("Account Confirmed");
        } else {
          this.messageService.add({severity:'error', summary:'Unknown Error', detail:'Some Problem Occured'});

          // this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }

  checkemailpresence() {
    let email = this.regForm.value.email;
    if(email) {
      this.expertservice.checkemailpresence(email).subscribe((data:any)=>{
        console.log("data without data",data);
        if(data){
          console.log("This mail is data",data);
          this.emailpresentflag = true;
          // this.mess ageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
          this.messageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
          return false;
        }
      },(error:any)=>{
        console.log("Error");
      })
    }
  }
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      console.log("PROVIDER_ID",GoogleLoginProvider.PROVIDER_ID);
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log("This is user data" ,userData);
        if(userData.provider=='google'){
          this.expertservice.checkgid(userData.id).subscribe((gdata:any)=>{

            if(gdata) {
              console.log("This is gid",gdata);
              localStorage.setItem('user_name',gdata.name);
              localStorage.setItem('user_email',gdata.email);
              localStorage.setItem('user_id',gdata._id);
              console.log("username"+ localStorage.user_name + "email" + localStorage.email);
              document.getElementById('close_main').click();
              this.router.navigate(['dashboard']);
            }
          },(gerror:any)=>{
            console.log("Thats gerror",gerror);
            if(gerror.status==400) {
              this.expertservice.savegoogleid(userData).subscribe((data:any)=>{
                if(userData.token) {
                  localStorage.setItem('token',userData.token);
                  localStorage.setItem('user_name',userData.name);
                  localStorage.setItem('user_email',userData.email);
                  localStorage.setItem('user_id',data._id);
                  document.getElementById('close_main').click();
                  this.router.navigate(['/dashboard']);
                }
              },(error:any)=>{
                console.log("Got error saving the google id",error);
              })
            }
          })
        }
        else if(userData.provider=='facebook') {
          this.expertservice.checkfbid(userData.id).subscribe((fbdata:any)=>{
            if(fbdata.status == 200) {
              console.log("This is fid",fbdata);
              localStorage.setItem('user_name',fbdata.name);
              localStorage.setItem('user_email',fbdata.email);
              localStorage.setItem('user_id',fbdata._id);
              console.log("username"+ localStorage.user_name + "email" + localStorage.email);
              document.getElementById('close_main').click();
              this.router.navigate(['dashboard']);
            }
          },(fberror:any)=>{
            this.expertservice.savefacebookid(userData).subscribe((data:any)=>{
              if(userData.token) {
                localStorage.setItem('token',userData.token);
                localStorage.setItem('user_name',userData.name);
                localStorage.setItem('user_email',userData.email);
                localStorage.setItem('user_id',data._id);
                document.getElementById('close_main').click();
                this.router.navigate(['/dashboard']);
              }
            },(error:any)=>{
              console.log("Got error saving the google id",error);
            })
          })
        }

      }
    );
  }

  register() {
    if(!this.emailpresentflag) {
      this.registerloading = true;
      let input = this.regForm.value ? this.regForm.value : '';
      console.log("This is the form value ",this.regForm.value);
      if(input) {
        this.expertservice.register_user(input).subscribe((data:any)=>{
          if(data.message){
            this.messageService.add({severity:'success', summary:'User Registered', detail:'Thank you for getting registered with us! Please Login'});
            setTimeout(()=>{
              document.getElementById('close_register').click();
              document.getElementById('login_btn').click();
            },3000);

          }
          this.registerloading= false;
          console.log("Successfully regustered ", data);
        },(error:any)=>{
          console.log("This is the error i n the registeratiuon ", error);
        })
      }
    }
    else{
      this.messageService.add({severity:'error', summary:'Email Exist', detail:'Please try another email'});
    }
  }

  articlecat(category) {
    console.log("you got category",category);
    this.router.navigate(['/articlecat',category]);
  
    setTimeout(()=>{
    this.globalservice.updatearticles_category('category changed');
  },500);
  }
  
}

