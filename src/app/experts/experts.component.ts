import { Component, OnInit } from '@angular/core';
import { ExpertService  } from '../providers/expert.service';
import { GlobalService } from '../providers/global.service';
import { Router,ActivatedRoute,Params } from '@angular/router';
@Component({
  selector: 'app-experts',
  templateUrl: './experts.component.html',
  styleUrls: ['./experts.component.css']
})
export class ExpertsComponent implements OnInit {
  articleData:any;
  projectionimageurl:String;
  userarticleData:any;
  expertData:any;
  expertimagesurl:string;
  postcount:number=0;
  expertcount:number=0;
  userid:any;
  search:any;
  selectedExpertise:any;
  expertlimit:number=0;
  constructor(public expertservice : ExpertService,public router:Router,public globalservice:GlobalService) {
  this.projectionimageurl  = this.globalservice.serverip() + 'articles/projections/';
  this.expertimagesurl = this.globalservice.serverip() + 'public/experts/';

  }
  ngOnInit() {
    this.getallexperts();
    this.userid = localStorage.user_id;
 
  }
  getallexperts(){
    var body={
      limit:this.expertlimit+8
    }
    this.expertservice.getallexperts(body).subscribe((data:any)=>{
      console.log("this isthe data",data);
      this.expertData = data.data;
      this.expertlimit=this.expertlimit+8
      if(localStorage.getItem('search')){
        this.searchName(localStorage.getItem('search'))
      }
    },(error:any)=>{
      console.log("error",error);
    })
  }
  
  searchArticle(){
    if(this.search){
    localStorage.setItem('search',this.search)
    }
    this.router.navigate(['/blog'])

  }

  searchVideos(){
    if(this.search){
      localStorage.setItem('search',this.search)
      }
      this.router.navigate(['/videos'])
  
  }
  
  searchName(event){
    console.log(event)
    // console.log((<HTMLInputElement>document.getElementById("mySearch")).value)
    var input, filter, ul, li, a, i;
    if(!localStorage.getItem('search')){
      input = (<HTMLInputElement>document.getElementById("mySearch")).value;
    }
    if(localStorage.getItem('search')){
      input = event;
    }
    console.log("iiiiiiiiiiiiiiiiii",input)
    this.search=input;
    filter = input.toUpperCase();
    console.log(filter)

    var body =
    {
      "name" : filter,
      "searchin" : "expert"
    }
  
      this.expertservice.getsearchedarticle(body).subscribe((data:any)=>{
        console.log("this is the first search",data);         
            this.expertData = data;

     },)



    ul = document.getElementById("myMenu");
    console.log(ul)
    li = ul.getElementsByClassName("item");
    localStorage.removeItem('search')
    console.log('liiiiiiiiiiii',li)
    console.log(li.length)
    setTimeout(function() {
      for (i = 0; i < li.length; i++) {
        console.log('particular lli', li[i])    
        a = li[i].getElementsByTagName("a")[0];
        console.log(li[i].getElementsByTagName("a")[0])
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
          console.log('show',li[i])
            li[i].style.display = "";
        } else {
          console.log('hidden',li[i])
            li[i].style.display = "none";
        }
      }
    },100);
  }

  onChange(event) {
    console.log(event)
    var input, filter, ul, li, a, i;
    input = event;
    filter = input.toUpperCase();
    console.log(filter)

    var body =
    {
      "name" : filter,
      "searchin" : "tags"
    }
  
      this.expertservice.getsearchedarticle(body).subscribe((data:any)=>{
        console.log("this is the first search of",data);         
            this.expertData = data;

     },)

    ul = document.getElementById("myMenu");
    console.log(ul)
    li = ul.getElementsByClassName("item");
    console.log('liiiiiiiiiiii',li)
    for (i = 0; i < li.length; i++) {
        console.log('particular lli', li[i])    
            a = li[i].getElementsByTagName("h5")[0];
        console.log(li[i].getElementsByTagName("h5")[0])
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
          console.log('show',li[i])
            li[i].style.display = "";
        } else {
          console.log('hidden',li[i])
            li[i].style.display = "none";
        }
    }
    }

}
